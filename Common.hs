--------------------------------------------------------------------------------
-- |
-- Module: Common
--
-- Contains various commonly used code. Currently includes vectors, rays,
-- colors, rectangles, boxes and some useful functions.
--
--------------------------------------------------------------------------------

module Common (
    -- * Constants
    epsilon,        -- :: Double

    -- * Colors
    Color,
    makeRGB,        -- :: Int -> Int -> Int -> Color

    -- * Rectangle
    Rectangle,
    makeRect,       -- :: (Real a) => (a, a) -> (a, a) -> Rectangle a

    pointInRect,    -- :: (Real a) => (a, a) -> Rectangle a -> Bool
    rectCorners,    -- :: (Real a) => Rectangle a -> ((a, a), (a, a))

    -- * Box
    Box,
    makeBox,            -- :: (Real a) => (a, a, a) -> (a, a, a) -> Box a

    allBoxCorners,      -- :: (Real a) => Box a -> [(a, a, a)]
    boxCorners,         -- :: (Real a) => Box a -> ((a, a, a), (a, a, a))
    boxNormal,          -- :: (Real a) => Box a -> Vector3 -> Vector3
    pointInBox,         -- :: (Real a) => (a, a, a) -> Box a -> Bool

    boxSurfaceArea,     -- :: (Real a) => Box a -> a
    joinBoxes,          -- :: (Real a) => Box a -> Box a -> Box a

    boxIntersectsBox,       -- :: Box Double -> Box Double -> Bool

    rayIntersectsBox,       -- :: Box Double -> Ray -> [Vector3]
    rayIntersectsBox',      -- :: Box Double -> Ray -> [Double]
    lineIntersectsBox,      -- :: Box Double -> Ray -> [Vector3]
    lineIntersectsBox',     -- :: Box Double -> Ray -> [Double]

    -- * Vectors
    Vector3,
    ColorVector,

    addV,               -- :: Vector3 -> Vector3 -> Vector3
    subV,               -- :: Vector3 -> Vector3 -> Vector3
    multV,              -- :: Vector3 -> Vector3 -> Vector3
    multScV,            -- :: Double -> Vector3 -> Vector3
    lengthV,            -- :: Vector3 -> Double
    lengthSqrV,         -- :: Vector3 -> Double
    dotProduct,         -- :: Vector3 -> Vector3 -> Double
    crossProduct,       -- :: Vector3 -> Vector3 -> Double
    crossVector,        -- :: Vector3 -> Vector3 -> Vector3
    normalizeV,         -- :: Vector3 -> Vector3
    negateV,            -- :: Vector3 -> Vector3

    addListV,           -- :: [Vector3] -> Vector3
    multListV,          -- :: [Vector3] -> Vector3

    distanceV,          -- :: Vector3 -> Vector3 -> Double
    vectorReflection,   -- :: Vector3 -> Vector3 -> Vector3

    -- * Rays
    Ray,
    makeRay,            -- :: Vector3 -> Vector3 -> Ray

    rayDirection,       -- :: Ray -> Vector3
    rayOrigin,          -- :: Ray -> Vector3

    intrPointPos,       -- :: Ray -> Double -> Vector3

    -- * Miscellaneous functions
    color2vector,   -- :: Color -> ColorVector
    color2tuple,    -- :: Color -> (Int, Int, Int)
    vector2color,   -- :: ColorVector -> Color

    floatsEqual,    -- :: Double -> Double -> Bool
    minMax,         -- :: (Ord a) => a -> a -> (a, a)
    partition,      -- :: [a] -> Int -> [[a]]
    restrict,       -- :: (Ord a) => a -> a -> a -> a
    slice,          -- :: [a] -> Int -> Int -> [a]

    -- * Predefined variables
    black,
    blue,
    cyan,
    gray,
    green,
    magenta,
    red,
    yellow,
    white
    ) where

import Data.List (foldl')

import Control.Parallel.Strategies (NFData, rnf)


--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

-- | Minimal accuracy of floating-point operations.
epsilon :: Double
epsilon = 1e-8


--------------------------------------------------------------------------------
-- COLORS
--------------------------------------------------------------------------------

-- | A type, representing a color. Currently supports only the RGB palette.
data Color = RGB Int Int Int
             deriving (Eq, Read, Show)

-- | This instance is needed for parallel computation.
instance NFData Color where
    rnf (RGB r g b) = r `seq` g `seq` b `seq` ()


-- | Creates a color in RGB palette.
makeRGB :: Int -> Int -> Int -> Color
makeRGB red green blue
    | red `seq` green `seq` blue `seq` False = undefined
    | red   < 0 || red   > 255 = error "Invalid RGB color component value."
    | green < 0 || green > 255 = error "Invalid RGB color component value."
    | blue  < 0 || blue  > 255 = error "Invalid RGB color component value."
    | otherwise                = RGB red green blue


-- Predefined colors.
black   = makeRGB   0   0   0
blue    = makeRGB   0   0 255
cyan    = makeRGB   0 255 255
gray    = makeRGB 128 128 128
green   = makeRGB   0 255   0
magenta = makeRGB 255   0 255
red     = makeRGB 255   0   0
yellow  = makeRGB 255 255   0
white   = makeRGB 255 255 255


--------------------------------------------------------------------------------
-- RECTANGLE
--------------------------------------------------------------------------------

-- | Rectangle type.
data (Real a) => Rectangle a = Rect (a, a) (a, a)
                               deriving(Eq, Read, Show)

-- | Creates a rectangle object from the coordinates of any two of its opposite
-- corners.
makeRect :: (Real a) => (a, a) -> (a, a) -> Rectangle a
makeRect (x1, y1) (x2, y2) = Rect (p1x, p1y) (p2x, p2y)
    where
    (p1x, p2x) = minMax x1 x2
    (p1y, p2y) = minMax y1 y2


-- | Returns True, if the specified point is within the rectangle.
pointInRect :: (Real a) => (a, a) -> Rectangle a -> Bool
pointInRect (x, y) (Rect (x1, y1) (x2, y2)) =
    ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2))

-- | Returns the coordinates of upper-left and lower-right corners of a given
-- rectangle.
rectCorners :: (Real a) => Rectangle a -> ((a, a), (a, a))
rectCorners (Rect a b) = (a, b)


--------------------------------------------------------------------------------
-- BOX
--------------------------------------------------------------------------------

-- | Box type.
data (Real a) => Box a = Box (a, a, a) (a, a, a)
                         deriving (Eq, Read, Show)

-- | Creates a box object from the coordinates of any two opposite corners
-- of the box.
makeBox :: (Real a) => (a, a, a) -> (a, a, a) -> Box a
makeBox (x1,y1,z1) (x2,y2,z2) = Box (p1x, p1y, p1z) (p2x, p2y, p2z)
    where
    (p1x, p2x) = minMax x1 x2
    (p1y, p2y) = minMax y1 y2
    (p1z, p2z) = minMax z1 z2


-- | Returns the coordinates of all eight corners of a given box.
allBoxCorners :: (Real a) => Box a -> [(a, a, a)]
allBoxCorners (Box (x1,y1,z1) (x2,z2,y2)) = result `seq` result
    where
    result = [(x, y, z) | x <- [x1,x2], y <- [y1,y2], z <- [z1,z2]]

-- | Returns the coordinates of upper-left-closest and lower-right-farthest
-- corners of a given box.
boxCorners :: (Real a) => Box a -> ((a, a, a), (a, a, a))
boxCorners (Box a b) = (a, b)


-- | Returns the normal to a box at the specified point.
--
-- TODO: Make this function generic.
boxNormal :: Box Double -> Vector3 -> Vector3
boxNormal (Box (x1,y1,z1) (x2,y2,z2)) (px,py,pz) =
         if (floatsEqual px x1) then (-1, 0, 0)
    else if (floatsEqual px x2) then ( 1, 0, 0)
    else if (floatsEqual py y1) then ( 0,-1, 0)
    else if (floatsEqual py y2) then ( 0, 1, 0)
    else if (floatsEqual pz z1) then ( 0, 0,-1)
    else                             ( 0, 0, 1)     -- pz == z2


-- | Returns True, if the specified point in the box.
pointInBox :: (Real a) => (a, a, a) -> Box a -> Bool
pointInBox (x, y, z) (Box (x1, y1, z1) (x2, y2, z2)) =
    ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2) && (z >= z1) && (z <= z2))


-- | Returns the surface area of a box.
boxSurfaceArea :: (Real a) => Box a -> a
boxSurfaceArea (Box (x1, y1, z1) (x2, y2, z2)) = area `seq` area
    where
    (a,b,c) = (x2-x1, y2-y1, z2-z1)
    area    = 2 * (a*b + a*c + b*c)


-- | Returns a box, which bounds two given boxes.
joinBoxes :: (Real a) => Box a -> Box a -> Box a
joinBoxes (Box (ox1,oy1,oz1) (ox2,oy2,oz2)) (Box (px1,py1,pz1) (px2,py2,pz2)) =
    makeBox (x1,y1,z1) (x2,y2,z2)
    where
    (x1, x2) = minMax (ox1 `min` px1) (ox2 `max` px2)
    (y1, y2) = minMax (oy1 `min` py1) (oy2 `max` py2)
    (z1, z2) = minMax (oz1 `min` pz1) (oz2 `max` pz2)


-- | Returns True, if two given boxes intersect or if one box is contained
-- within another box.
boxIntersectsBox :: Box Double -> Box Double -> Bool
boxIntersectsBox (Box (ax1,ay1,az1) (ax2,ay2,az2))
                 (Box (bx1,by1,bz1) (bx2,by2,bz2)) =
    xOverlap `seq` yOverlap `seq` zOverlap `seq` xOverlap && yOverlap && zOverlap
    where
    xOverlap = not ((ax2 < bx1) || (bx2 < ax1))
    yOverlap = not ((ay2 < by1) || (by2 < ay1))
    zOverlap = not ((az2 < bz1) || (bz2 < az1))


-- | Checks whether a ray intersects some box. A list of intersection points
-- is returned.
--
-- TODO: Make generic.
rayIntersectsBox :: Box Double -> Ray -> [Vector3]
rayIntersectsBox box ray = map (intrPointPos ray) (rayIntersectsBox' box ray)


-- | Checks whether a ray intersects some box. A list of distances to the
-- intersections is returned.
--
-- TODO: Make generic.
rayIntersectsBox' :: Box Double -> Ray -> [Double]
rayIntersectsBox' box ray = filter (>0) (lineIntersectsBox' box ray)


-- | Checks wheter a line (represented by a ray) intersects some box. A list
-- of intersection points is returned.
--
-- TODO: Make generic.
lineIntersectsBox :: Box Double -> Ray -> [Vector3]
lineIntersectsBox box ray = map (intrPointPos ray) (lineIntersectsBox' box ray)


-- | Checks wheter a line (represented by a ray) intersects some box. A list
-- of distances to intersections is returned.
--
-- TODO: Make generic.
lineIntersectsBox' :: Box Double -> Ray -> [Double]
lineIntersectsBox' (Box (a1,a2,a3) (b1,b2,b3)) ray
    | nearDist `seq` farDist `seq` isParallel `seq` False = undefined
    | otherwise = if isParallel || (nearDist > farDist)
                    then []
                    else [nearDist, farDist]
    where
    -- Intersections are found by checking whether a ray intersects "slabs" of a
    -- box. Unless the ray is parallel to the slab, there are always two
    -- intersections. By calculating all of these intersections and comparing
    -- them, it is possible to find whether a line intersects a box and where.
    (o1, o2, o3) = rayOrigin ray
    (d1, d2, d3) = rayDirection ray

    (xMin, xMax) = minMax ((a1 - o1) / d1) ((b1 - o1) / d1)
    (yMin, yMax) = minMax ((a2 - o2) / d2) ((b2 - o2) / d2)
    (zMin, zMax) = minMax ((a3 - o3) / d3) ((b3 - o3) / d3)

    nearDist = xMin `max` yMin `max` zMin
    farDist  = xMax `min` yMax `min` zMax

    -- This variable is true, when the ray is parallel to the box and does not
    -- intersect it.
    isParallel = not (((not (floatsEqual d1 0.0)) || ((o1 > a1) && (o1 < b1))) &&
                      ((not (floatsEqual d2 0.0)) || ((o2 > a2) && (o2 < b2))) &&
                      ((not (floatsEqual d3 0.0)) || ((o3 > a3) && (o3 < b3))))

--------------------------------------------------------------------------------
-- VECTOR OF THREE ELEMENTS
--------------------------------------------------------------------------------

-- | Defines a vector with three components.
type Vector3 = (Double, Double, Double)

-- | A vector, which represents a color.
type ColorVector = Vector3


-- | Adds two vectors together.
addV :: Vector3 -> Vector3 -> Vector3
addV (a1,b1,c1) (a2,b2,c2)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (a1+a2, b1+b2, c1+c2)

-- | Subtracts one vector from another.
subV :: Vector3 -> Vector3 -> Vector3
subV (a1,b1,c1) (a2,b2,c2)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (a1-a2, b1-b2, c1-c2)

-- | Multiplies two vectors together.
multV :: Vector3 -> Vector3 -> Vector3
multV (a1,b1,c1) (a2,b2,c2)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (a1*a2, b1*b2, c1*c2)

-- | Multiplies a vector by a scalar.
multScV :: Double -> Vector3 -> Vector3
multScV r (a,b,c)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (a*r, b*r, c*r)

-- | Returns the length of a vector.
lengthV :: Vector3 -> Double
lengthV (a,b,c) = sqrt (a*a + b*b + c*c)

-- | Returns the length of a vector squared.
lengthSqrV :: Vector3 -> Double
lengthSqrV (a,b,c) = a*a + b*b + c*c

-- | Returns the dot product of two vectors.
dotProduct :: Vector3 -> Vector3 -> Double
dotProduct (a1,b1,c1) (a2,b2,c2) = a1*a2 + b1*b2 + c1*c2

-- | Returns the cross product of two vectors.
crossProduct :: Vector3 -> Vector3 -> Double
crossProduct (a1,b1,c1) (a2,b2,c2) = b1*c2 - c1*b2 + c1*a2 - a1*c2 + a1*b2 - b1*a2

-- | Returns a vector, which is perpendicular to the plane, defined by the
-- given vectors (the proper cross product).
crossVector :: Vector3 -> Vector3 -> Vector3
crossVector (a1,b1,c1) (a2,b2,c2)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (b1*c2 - c1*b2, c1*a2 - c2*a1, a1*b2 - b1*a2)

-- | Normalizes a vector.
normalizeV :: Vector3 -> Vector3
normalizeV v = multScV (1 / (lengthV v)) v

-- | Inverses the direction of a vector.
negateV :: Vector3 -> Vector3
negateV (a,b,c)
    | x `seq` y `seq` z `seq` False = undefined
    | otherwise = (x, y, z)
    where
    (x, y, z) = (-a,-b,-c)


-- | Adds a list of vectors together.
addListV :: [Vector3] -> Vector3
addListV vs = foldl' addV (0,0,0) vs

-- | Multiplies a list of vectors together.
multListV :: [Vector3] -> Vector3
multListV vs = foldl' multV (1,1,1) vs


-- | Returns the distance between two point in 3D space (in this case vectors
-- are interpreted as coordinates).
distanceV :: Vector3 -> Vector3 -> Double
distanceV (x1, y1, z1) (x2, y2, z2) = sqrt ((x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2)

-- | Returns the direction of a vector, after it has reflected off some plane
-- with specified normal. Assumes that both input vectors are normalized.
vectorReflection :: Vector3 -> Vector3 -> Vector3
vectorReflection normal dir = result `seq` result
    where
    result = subV dir (multScV (2 * dotProduct dir normal) normal)


--------------------------------------------------------------------------------
-- RAYS
--------------------------------------------------------------------------------

-- | The ray type. First vector denotes the origin of the ray, second one
-- shows its direction.
data Ray = Ray Vector3 Vector3
           deriving (Eq, Read, Show)

-- | Creates a ray from two vectors.
makeRay :: Vector3 -> Vector3 -> Ray
makeRay orig dir = orig `seq` dir1 `seq` Ray orig dir1
    where
    dir1 = normalizeV dir


-- | Returns the direction of a ray.
rayDirection :: Ray -> Vector3
rayDirection (Ray orig dir) = dir

-- | Returns the origin of a ray.
rayOrigin :: Ray -> Vector3
rayOrigin (Ray orig dir) = orig


-- | Returns the coordinates of the point, where a ray intersects some
-- object. Takes the ray in question and the distance to intersection.
intrPointPos :: Ray -> Double -> Vector3
intrPointPos ray dist = addV (rayOrigin ray) (multScV dist (rayDirection ray))


--------------------------------------------------------------------------------
-- MISCELLANEOUS FUNCTIONS
--------------------------------------------------------------------------------

-- | Converts a RGB color into a vector.
color2vector :: Color -> ColorVector
color2vector (RGB r g b) = a1 `seq` b1 `seq` c1 `seq` (a1, b1, c1)
    where
    a1 = (fromIntegral r) / 255
    b1 = (fromIntegral g) / 255
    c1 = (fromIntegral b) / 255

-- | Converts a color into a tuple. The return value depends on type of color,
-- so RGB color will return a tuple of RGB values.
color2tuple :: Color -> (Int, Int, Int)
color2tuple (RGB r g b) = (r, g, b)

-- | Converts a vector, whose elements are between 0.0 and 1.0, into an RGB
-- color.
vector2color :: ColorVector -> Color
vector2color (a,b,c) = red `seq` green `seq` blue `seq` makeRGB red green blue
    where
    red   = floor (restrict 0 255 (a * 255))
    green = floor (restrict 0 255 (b * 255))
    blue  = floor (restrict 0 255 (c * 255))


-- | Compares two floating point numbers and return True, if their
-- difference is less than 'epsilon'.
--
-- TODO: Make generic.
floatsEqual :: Double -> Double -> Bool
floatsEqual x y = (abs (x-y)) < epsilon

-- | Takes two values and returns a pair, where the first element is smaller of
-- the two values, the second element is the larger one.
minMax :: (Ord a) => a -> a -> (a, a)
minMax x y = if (x > y) then (y, x) else (x, y)

-- | Splits a list into sub-lists of specified size.
partition :: [a] -> Int -> [[a]]
partition xs n
    | length xs <= n = [xs]
    | otherwise      = (take n xs) : (partition (drop n xs) n)

-- | Makes sure that the given variable is within the specified interval.
restrict :: (Ord a) => a -> a -> a -> a
restrict lBound uBound x
    | x < lBound = lBound
    | x > uBound = uBound
    | otherwise  = x

-- | Returns a slice of the list.
slice :: [a] -> Int -> Int -> [a]
slice xs from to = (drop from . take (to+1)) xs
