--------------------------------------------------------------------------------
-- |
-- Module: Material
--
-- Defines various materials, which can be applied to primitives to define
-- their appearance.
--
--------------------------------------------------------------------------------

module Material (
    -- * Material
    Material,

    makeMaterial,       -- :: Color -> Texture -> Double -> Double -> Double -> Double -> Double -> Double -> Int -> Material

    noMaterial,         -- :: Material
    solidMaterial,      -- :: Color -> Material
    textureMaterial,    -- :: Texture -> Material

    matColor,           -- :: Material -> ColorVector
    matDensity,         -- :: Material -> Double
    matDiffusion,       -- :: Material -> Double
    matReflection,      -- :: Material -> Double
    matRefraction,      -- :: Material -> Double
    matRefrIndex,       -- :: Material -> Double
    matShininess,       -- :: Material -> Int
    matSpecular,        -- :: Material -> Double
    matTexture          -- :: Material -> Texture

    ) where

import Bitmap
import Common
import Textures


--------------------------------------------------------------------------------
-- MATERIAL TYPE
--------------------------------------------------------------------------------

-- | Definition of material type. Arguments define the color of the material,
-- its texture and its diffusion, specular, reflection and refraction ratings, the
-- refraction index, material density and shininess rating.
data Material = Mat Vector3 Texture Double Double Double Double Double Double Int
                deriving(Eq, Read, Show)


-- | Creates a material from given color, texture and diffusion, reflection
-- and refraction ratings, the refraction index, material density and shininess
-- rating.
makeMaterial :: Color -> Texture -> Double -> Double -> Double -> Double -> Double -> Double -> Int -> Material
makeMaterial col tex diff spec refl refr refrIdx dens shine
    | (diff < 0.0) || (diff > 1.0) = error "Invalid diffusion rating."
    | (spec < 0.0) || (spec > 1.0) = error "Invalid specular rating."
    | (refl < 0.0) || (refl > 1.0) = error "Invalid reflection rating."
    | (refr < 0.0) || (refr > 1.0) = error "Invalid refraction rating."
    | (refrIdx <= 0.0)             = error "Invalid refraction index."
    | (dens <= 0.0)                = error "Invalid density."
    | (shine < 0)                  = error "Invalid shininess rating."
    | otherwise                    = Mat (color2vector col) tex diff spec refl refr refrIdx dens shine


-- | Default blank material.
noMaterial :: Material
noMaterial = makeMaterial white noTexture 1.0 0.0 0.0 0.0 1.0 1.0 20

-- | Creates a solid material with specified color.
solidMaterial :: Color -> Material
solidMaterial color = makeMaterial color noTexture 1.0 0.0 0.0 0.0 1.0 1.0 20

-- | Creates a solid material with a texture.
textureMaterial :: Texture -> Material
textureMaterial tex = makeMaterial white tex 1.0 0.0 0.0 0.0 1.0 1.0 20


-- | Returns the color of the material.
matColor :: Material -> ColorVector
matColor (Mat col _ _ _ _ _ _ _ _) = col

-- | Returns the density of the material.
matDensity :: Material -> Double
matDensity (Mat _ _ _ _ _ _ _ dens _) = dens

-- | Returns the diffusion rating of the material.
matDiffusion :: Material -> Double
matDiffusion (Mat _ _ diff _ _ _ _ _ _) = diff

-- | Returns the reflection rating of the material.
matReflection :: Material -> Double
matReflection (Mat _ _ _ _ refl _ _ _ _) = refl

-- | Returns the refraction rating of the material.
matRefraction :: Material -> Double
matRefraction (Mat _ _ _ _ _ refr _ _ _) = refr

-- | Returns the refraction index of the material.
matRefrIndex :: Material -> Double
matRefrIndex (Mat _ _ _ _ _ _ refrIdx _ _) = refrIdx

-- | Returns the shininess rating of the material.
matShininess :: Material -> Int
matShininess (Mat _ _ _ _ _ _ _ _ shine) = shine

-- | Returns the specular rating of the material.
matSpecular :: Material -> Double
matSpecular (Mat _ _ _ spec _ _ _ _ _) = spec

-- | Returns the texture of the material.
matTexture :: Material -> Texture
matTexture (Mat _ tex _ _ _ _ _ _ _) = tex


