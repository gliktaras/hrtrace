# hrtrace

A raytracer, built in Haskell. I have built this project from scratch for the
INF1-FP programming competition in the University of Edinburgh.

Features:

* Plane, sphere and axis-aligned box primitives.
* Point light sources and Phong lighting model.
* Reflections and refractions.
* Texture mapping on all primitives with bilinear filtering.
* Free camera.
* Antialiasing.
* Kd-trees are used for closest intersection checks.
* [Constructive solid geometry](https://en.wikipedia.org/wiki/Constructive_solid_geometry).
* Parallel computation.

**WARNING**: This code has been written with GHC 6.10.4 and it does not work
with the current version of GHC. If you want to get this to run, you will
either have to use that particular version of GHC or update the code to match.
The instructions below come from the original README.


## Building

Build requirements:

* GHC 6.10.4.
* "parallel" cabal package.

Build procedure:

    git clone https://gliktaras@bitbucket.org/gliktaras/hrtrace.git
    cd hrtrace
    make


## Usage

Run `./raytracer` to have the raytracer render the [Informatics
Forum](https://en.wikipedia.org/wiki/Informatics_Forum).

Run `./raytracer --render-tests` to have the raytracer render test images.

Other command line options are:

* *--no-aa* - Turn off the antialiasing of the competition image.
* *--no-textures* - Turn off textures of the competition image.
* *--size=N* - Set the size of the competition image to NxN. Default value is
  1000.

To have the raytracer use multiple CPU cores, append `+RTS -Nx` to the
invocation command, where `x` is the amount of cores you want to dedicate to
the program. Note that this has to go _after_ the above options.


## Documentation

To generate Haddock documentation of the project, run `make docs`.


## Performance

The raytracer takes about 3 minutes to render a 1000x1000 antialiased, textured
image of the Informatics Forum on a DICE machine (i.e. one of the computers
that was provided by the university for the informatics students in their labs
at the time of writing), using only a single core.


## Special Thanks

* Jonas Simanavicius, who helped me to produce the textures for the Forum.
* Texture of planet earth was taken from http://www.oera.net/How2/TextureMaps2.htm.
* The sky texture was taken from http://www.zooboing.com.


## Licensing

This project uses the BSD 2-Clause license.
