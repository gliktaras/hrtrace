--------------------------------------------------------------------------------
-- |
-- Module: Primitives
--
-- Defines objects to be rendered and their properties. Currently has a sphere,
-- a plane, an axis-aligned box, a point light and portals defined.
--
--------------------------------------------------------------------------------

module Primitives (
    -- * Types
    ComposeType(..),
    Intersection,
    Primitive,

    -- * Functions on intersections
    makeMiss,           -- :: Intersection

    isMiss,             -- :: Intersection -> Bool
    isHit,              -- :: Intersection -> Bool
    isOutsideHit,       -- :: Intersection -> Bool
    isInsideHit,        -- :: Intersection -> Bool

    intrDistance,       -- :: Intersection -> Double
    intrNormal,         -- :: Intersection -> Vector3
    intrPrimitive,      -- :: Intersection -> Primitive
    intrPoint,          -- :: Ray -> Intersection -> Vector3
    replaceIntrPrim,    -- :: Primitive -> Intersection -> Intersection

    -- * Primitive creation
    makeAABox,          -- :: Box Double -> Material -> Primitive
    makeComposite,      -- :: ComposeType -> Primitive -> Primitive -> Primitive
    makePlane,          -- :: Vector3 -> Double -> Material -> Primitive
    makeSphere,         -- :: Vector3 -> Double -> Material -> Primitive
    makePointLight,     -- :: Vector3 -> Color -> Primitive
    makePointLight',    -- :: Primitive -> Color -> Primitive
    makePortal,         -- :: Primitive -> Vector3 -> Primitive

    -- * Primitive filtering
    isLight,            -- :: Primitive -> Bool

    isAABox,            -- :: Primitive -> Bool
    isComposite,        -- :: Primitive -> Bool
    isPlane,            -- :: Primitive -> Bool
    isPointLight,       -- :: Primitive -> Bool
    isPortal,           -- :: Primitive -> Bool
    isSphere,           -- :: Primitive -> Bool

    -- * Intersection tests
    boxIntersectsPrim,      -- :: Primitive -> Box Double -> Bool
    rayIntersectsPrim,      -- :: Primitive -> Ray -> [Intersection]

    -- * Other functions on primitives
    pointInPrim,        -- :: Primitive -> Vector3 -> Bool

    getBoundingBox,     -- :: Primitive -> Box Double
    getPtLightSource,   -- :: Primitive -> Vector3
    getPortalDest,      -- :: Primitive -> Ray -> Ray

    -- * Primitives' material functions
    getMaterial,        -- :: Primitive -> Material

    getDensity,         -- :: Primitive -> Double
    getDiffuse,         -- :: Primitive -> Double
    getMaterialColor,   -- :: Primitive -> ColorVector
    getReflection,      -- :: Primitive -> Double
    getRefraction,      -- :: Primitive -> Double
    getRefrIndex,       -- :: Primitive -> Double
    getShininess,       -- :: Primitive -> Int
    getSpecular,        -- :: Primitive -> Double
    getTextureColor,    -- :: Primitive -> Vector3 -> ColorVector

    ) where

import Common
import Material
import Textures

import Data.List (sort)


--------------------------------------------------------------------------------
-- INTERSECTION TYPE
--------------------------------------------------------------------------------

-- | Enumeration, defining the type of ray-primitive intersection. Constructors,
-- denoting an intersection ('Outside' and 'Inside') take a primitive, distance
-- to it and a normal to the point of intersection as parameters.
data Intersection = Miss                                -- ^ No intersection.
                  | Outside !Primitive !Double Vector3  -- ^ Intersection with outside.
                  | Inside  !Primitive !Double Vector3  -- ^ Intersection from inside.
                  deriving (Eq, Read, Show)

-- | Instance of Ord of 'Intersection' type. Intersections are ordered by
-- their distances to the intersection. Misses have infinite distance.
instance Ord Intersection where
    Miss <= Miss = True
    _    <= Miss = True
    Miss <= _    = False
    i1   <= i2    = (intrDistance i1) <= (intrDistance i2)


-- | Returns an intersection object, denoting a miss.
makeMiss :: Intersection
makeMiss = Miss


-- | Returns, whether the given object denotes a miss.
isMiss :: Intersection -> Bool
isMiss (Miss) = True
isMiss _      = False

-- | Returns, whether the given object denotes an intersection.
isHit :: Intersection -> Bool
isHit (Miss) = False
isHit _      = True

-- | Returns True, if the given object denotes an outside intersection.
isOutsideHit :: Intersection -> Bool
isOutsideHit (Outside _ _ _) = True
isOutsideHit _               = False

-- | Returns True, if the given object denotes an inside intersection.
isInsideHit :: Intersection -> Bool
isInsideHit (Inside _ _ _) = True
isInsideHit _              = False


-- | Returns the distance to an intersection.
intrDistance :: Intersection -> Double
intrDistance (Outside _ d _) = d
intrDistance (Inside  _ d _) = d
intrDistance _               = error "No intersection."

-- | Returns the normal at the intersection.
intrNormal :: Intersection -> Vector3
intrNormal (Outside _ _ n) = n
intrNormal (Inside  _ _ n) = n
intrNormal _               = error "No intersection."

-- | Returns the primitive, which has been intersected.
intrPrimitive :: Intersection -> Primitive
intrPrimitive (Outside p _ _) = p
intrPrimitive (Inside  p _ _) = p
intrPrimitive _               = error "No intersection."


-- | Returns the coordinates of the point, where a ray intersects some
-- primitive.
intrPoint :: Ray -> Intersection -> Vector3
intrPoint ray intr
    | isMiss intr = error "No intersection."
    | otherwise   = intrPointPos ray (intrDistance intr)


-- | Replaces the current primitive of an intersection with a new one. For
-- internal use only.
replaceIntrPrim :: Primitive -> Intersection -> Intersection
replaceIntrPrim p (Miss)          = Miss
replaceIntrPrim p (Outside _ d n) = Outside p d n
replaceIntrPrim p (Inside  _ d n) = Inside p d n


--------------------------------------------------------------------------------
-- SCENE PRIMITIVES
--------------------------------------------------------------------------------

-- | An enumeration, which defines how composite objects should be combined.
data ComposeType = CompUnion    -- ^ Set union (X or Y).
                 | CompIntr     -- ^ Set intersection (X and Y).
                 | CompDiff     -- ^ Set difference (X and not Y).
                 deriving (Eq, Read, Show)

-- | Defines all primitives, which can be rendered.
data Primitive =
    -- | The axis-aligned box.
    AABox (Box Double) Material
    -- | The plane. It is defined by its normal and distance to the origin.
  | Plane Vector3 Double Material
    -- | The sphere. Its defined by the location of its centre and radius.
  | Sphere Vector3 Double Material

    -- | Point light source. Arguments specify the sphere to turn into a light
    -- source and color of the light. Properties of the sphere do not affect the
    -- way the light is handled.
  | PointLight Primitive ColorVector
    -- | The portal. It takes another primitive as its argument and gives it
    -- the properties of a portal. Any incoming rays are moved to another
    -- location (possibly with a changed direction).
  | Portal Primitive Vector3
    -- | The composite object. It takes two other primitives and combines them
    -- using set operations. This last parameter is the bounding box of the
    -- object.
  | Composite ComposeType Primitive Primitive (Box Double)
  deriving(Eq, Read, Show)


--------------------------------------------------------------------------------
-- PRIMITIVE CONSTRUCTION FUNCTIONS
--------------------------------------------------------------------------------

-- | Creates a box. The arguments are the box itself and its material.
makeAABox :: Box Double -> Material -> Primitive
makeAABox box mat = AABox box mat

-- | Creates a plane. The arguments, in order, are normal of the plane, its
-- distance from the origin and the material of the plane.
makePlane :: Vector3 -> Double -> Material -> Primitive
makePlane normal dist mat = Plane (normalizeV normal) dist mat

-- | Creates a sphere. The arguments, in order, are centre of the sphere, its
-- radius and material.
makeSphere :: Vector3 -> Double -> Material -> Primitive
makeSphere centre radius mat
    | radius <= 0.0 = error "Sphere radius cannot be negative."
    | otherwise     = Sphere centre radius mat


-- | Creates a point light source with default appearance (small plain
-- sphere). It takes the location of the light and its color as arguments.
makePointLight :: Vector3 -> Color -> Primitive
makePointLight pos col = makePointLight' (makeSphere pos 0.1 (solidMaterial col)) col

-- | Creates a point light source. The arguments, in order, are the sphere that
-- corresponds the light source and the color of the light.
makePointLight' :: Primitive -> Color -> Primitive
makePointLight' p@(Sphere _ _ _) col = PointLight p (color2vector col)
makePointLight' _                col =
    error "Only spheres can be used to create point light sources."


-- | Creates a portal. This function takes a primitive, which will be
-- transformed into a portal, and ray's offset.
makePortal :: Primitive -> Vector3 -> Primitive
makePortal prim off
    | isLight prim  = error "A light source cannot be turned into a portal."
    | isPortal prim = error "Another portal cannot be turned into a portal."
    | otherwise     = Portal prim off


-- | Creates a composite object. It takes a 'ComposeType' value to determine
-- the method of composition and two primitives to compose.
makeComposite :: ComposeType -> Primitive -> Primitive -> Primitive
makeComposite cType p1 p2 = Composite cType p1 p2 bbox
    where
    bbox = joinBoxes (getBoundingBox p1) (getBoundingBox p2)


--------------------------------------------------------------------------------
-- PRIMITIVE FILTERING FUNCTIONS
--------------------------------------------------------------------------------

-- | Returns True, if some primitive is a light source.
isLight :: Primitive -> Bool
isLight (PointLight _ _) = True
isLight _                = False


-- | Returns True, if some primitive is a box.
isAABox :: Primitive -> Bool
isAABox (AABox _ _)  = True
isAABox (Portal p _) = isAABox p
isAABox _            = False

-- | Returns True, if some primitive is a composite object.
isComposite :: Primitive -> Bool
isComposite (Composite _ _ _ _) = True
isComposite _                   = False

-- | Returns True, if some primitive is a plane.
isPlane :: Primitive -> Bool
isPlane (Plane _ _ _) = True
isPlane (Portal p _)  = isPlane p
isPlane _             = False

-- | Returns True, if some primitive is a point light.
isPointLight :: Primitive -> Bool
isPointLight (PointLight _ _) = True
isPointLight _                = False

-- | Returns True, if some primitive is a portal.
isPortal :: Primitive -> Bool
isPortal (Portal _ _) = True
isPortal _            = False

-- | Returns True, if some primitive is a sphere.
isSphere :: Primitive -> Bool
isSphere (Sphere _ _ _) = True
isSphere (Portal p _)   = isSphere p
isSphere _              = False


--------------------------------------------------------------------------------
-- BOX INTERSECTION CHECKS
--------------------------------------------------------------------------------

-- | Returns, whether some primitive intersects the given box.
boxIntersectsPrim :: Primitive -> Box Double -> Bool
boxIntersectsPrim (AABox box' _)        box = boxIntersectsBox box' box
boxIntersectsPrim p@(Composite _ _ _ _) box = boxIntersectsComposite p box
boxIntersectsPrim p@(Plane _ _ _)       box = boxIntersectsPlane p box
boxIntersectsPrim (PointLight prim _)   box = boxIntersectsPrim prim box
boxIntersectsPrim p@(Sphere _ _ _)      box = boxIntersectsSphere p box
boxIntersectsPrim (Portal prim _)       box = boxIntersectsPrim prim box


-- | Returns, whether some box intersects a composite object.
boxIntersectsComposite :: Primitive -> Box Double -> Bool
boxIntersectsComposite (Composite cType p1 p2 _) box =
    case cType of
        CompUnion -> (boxIntersectsPrim p1 box) || (boxIntersectsPrim p2 box)
        CompIntr  -> (boxIntersectsPrim p1 box) && (boxIntersectsPrim p2 box)
        -- This below is not as optimal is it could be.
        CompDiff  -> (boxIntersectsPrim p1 box)


-- | Returns, whether some box intersects the given plane.
boxIntersectsPlane :: Primitive -> Box Double -> Bool
boxIntersectsPlane (Plane (a,b,c) dist _) box = not (all (== head cSide) (tail cSide))
    where
    d = dist * (sqrt (a*a + b*b + c*c))
    cSide = [(a*x + b*y + c*z + d) > 0 | (x, y, z) <- (allBoxCorners box)]


-- | Returns, whether some box intersects the given sphere.
--
-- TODO: Use proper comparison.
boxIntersectsSphere :: Primitive -> Box Double -> Bool
boxIntersectsSphere (Sphere (x,y,z) rad _) box = boxIntersectsBox box' box
    where
    box' = makeBox (x-rad, y-rad, z-rad) (x+rad, y+rad, z+rad)


--------------------------------------------------------------------------------
-- RAY INTERSECTION CHECKS
--------------------------------------------------------------------------------

-- | Returns, whether some ray intersects the given primitive.
rayIntersectsPrim :: Primitive -> Ray -> [Intersection]
rayIntersectsPrim p@(AABox _ _)         ray = rayIntersectsAABox p ray
rayIntersectsPrim p@(Composite _ _ _ _) ray = rayIntersectsComposite p ray
rayIntersectsPrim p@(Plane _ _ _)       ray = rayIntersectsPlane p ray
rayIntersectsPrim p@(PointLight prim _) ray = map (replaceIntrPrim p) (rayIntersectsPrim prim ray)
rayIntersectsPrim p@(Sphere _ _ _)      ray = rayIntersectsSphere p ray
rayIntersectsPrim p@(Portal prim _)     ray = map (replaceIntrPrim p) (rayIntersectsPrim prim ray)


-- | Check whether a ray intersects some box primitive.
rayIntersectsAABox :: Primitive -> Ray -> [Intersection]
rayIntersectsAABox p@(AABox box _) ray =
    case (rayIntersectsBox' box ray) of
        []    -> []
        [x]   -> let xNormal = negateV (boxNormal box (intrPointPos ray x))
                 in  [Inside p x xNormal]
        [x,y] -> let xNormal = (boxNormal box (intrPointPos ray x))
                     yNormal = negateV (boxNormal box (intrPointPos ray y))
                 in  [Outside p x xNormal, Inside p y yNormal]


-- | Returns all intersections with a composite object.
rayIntersectsComposite :: Primitive -> Ray -> [Intersection]
rayIntersectsComposite (Composite cType p1 p2 _) ray =
    case cType of
        CompUnion -> sort ((filter (intrOutside p2) p1intr) ++ (filter (intrOutside p1) p2intr))
        CompIntr  -> sort ((filter (intrInside  p2) p1intr) ++ (filter (intrInside  p1) p2intr))
        CompDiff  -> sort ((filter (intrOutside p2) p1intr) ++ (filter (intrInside  p1) p2intr))
    where
    p1intr = case (rayIntersectsBox (getBoundingBox p1) ray) of
                [] -> []
                _  -> rayIntersectsPrim p1 ray
    p2intr = case (rayIntersectsBox (getBoundingBox p2) ray) of
                [] -> []
                _  -> rayIntersectsPrim p2 ray

    intrInside  p i = pointInPrim p (intrPoint ray i)
    intrOutside p i = not (intrInside p i)


-- | Returns, whether the given ray intersects some plane.
rayIntersectsPlane :: Primitive -> Ray -> [Intersection]
rayIntersectsPlane p@(Plane normal dist _) ray =
    if (den == 0) || (iDist <= 0)
      then []
      else [Outside p iDist normal]
    where
    den   = dotProduct (rayDirection ray) normal
    iDist = ((-dist) - (dotProduct (rayOrigin ray) normal)) / den


-- | Returns, whether the given ray intersects some sphere.
rayIntersectsSphere :: Primitive -> Ray -> [Intersection]
rayIntersectsSphere p@(Sphere ctr@(c1,c2,c3) radius _) ray = result
    where
    (o1, o2, o3) = rayOrigin ray
    (d1, d2, d3) = rayDirection ray

    -- Coefficients of the quadratic equation.
    b   = 2 * (d1*(o1-c1) + d2*(o2-c2) + d3*(o3-c3))
    c   = (o1-c1)^2 + (o2-c2)^2 + (o3-c3)^2 - radius^2
    dis = b^2 - 4*c

    -- Solutions of the quadratic equation.
    sol1 = (((-b) - (sqrt dis)) / 2.0)
    sol2 = (((-b) + (sqrt dis)) / 2.0)

    result = if dis > 0
               then case (sol1 <= 0, sol2 <= 0) of
                        (True,  True ) -> []
                        (True,  False) -> let s2nrm = sphereNormal (intrPointPos ray sol2)
                                          in  [Inside p sol2 (negateV s2nrm)]
                        (False, False) -> let s1nrm = sphereNormal (intrPointPos ray sol1)
                                              s2nrm = sphereNormal (intrPointPos ray sol2)
                                          in  [Outside p sol1 s1nrm, Inside p sol2 (negateV s2nrm)]
               else []

    sphereNormal pt = normalizeV (subV pt ctr)


--------------------------------------------------------------------------------
-- POINT IN PRIMITIVE FUNCTIONS
--------------------------------------------------------------------------------

-- | Returns True, if a given point is contained within the primitive.
pointInPrim :: Primitive -> Vector3 -> Bool
pointInPrim p@(AABox box _)       pt = pointInBox pt box
pointInPrim p@(Composite _ _ _ _) pt = pointInComposite p pt
pointInPrim p@(Plane _ _ _)       pt = pointInPlane p pt
pointInPrim p@(PointLight prim _) pt = pointInPrim prim pt
pointInPrim p@(Sphere _ _ _)      pt = pointInSphere p pt
pointInPrim p@(Portal prim _)     pt = pointInPrim prim pt


-- | Returns True, if a given point is contained within some composite object.
pointInComposite :: Primitive -> Vector3 -> Bool
pointInComposite (Composite cType p1 p2 _) pt =
    case cType of
        CompUnion -> (p1cont) || (p2cont)
        CompIntr  -> (p1cont) && (p2cont)
        CompDiff  -> (p1cont) && (not p2cont)
    where
    p1cont = case (pointInBox pt (getBoundingBox p1)) of
                False -> False
                True  -> pointInPrim p1 pt
    p2cont = case (pointInBox pt (getBoundingBox p2)) of
                False -> False
                True  -> pointInPrim p2 pt


-- | Returns True, if a given is on the specified plane.
pointInPlane :: Primitive -> Vector3 -> Bool
pointInPlane (Plane (a,b,c) d _) (x,y,z) = floatsEqual (a*x + b*y + c*z + d) 0.0


-- | Returns True, if a given point is contained within some sphere.
pointInSphere :: Primitive -> Vector3 -> Bool
pointInSphere (Sphere (cx,cy,cz) rad _) (px,py,pz) = distSqr <= (rad ^ 2)
    where
    distSqr = (px - cx) ^ 2 + (py - cy) ^ 2 + (pz - cz) ^ 2


--------------------------------------------------------------------------------
-- OTHER PRIMITIVE INFORMATION
--------------------------------------------------------------------------------

-- | Returns the bounding box of the specified primitive.
--
-- Composite object has a bounding box, which covers all of its components. It
-- is NOT a minimal bounding box.
getBoundingBox :: Primitive -> Box Double
getBoundingBox (AABox box _)            = box
getBoundingBox (Composite _ p1 p2 bbox) = bbox
getBoundingBox (Plane _ _ _)            = makeBox (-1e300, -1e300, -1e300) (1e300, 1e300, 1e300)
getBoundingBox (PointLight prim _)      = getBoundingBox prim
getBoundingBox (Sphere (x,y,z) r _)     = makeBox (x-r, y-r, z-r) (x+r, y+r, z+r)
getBoundingBox (Portal prim _)          = getBoundingBox prim


-- | Returns the location of the point light source.
getPtLightSource :: Primitive -> Vector3
getPtLightSource (PointLight (Sphere ctr _ _) _) = ctr
getPtLightSource _                               = error "Not a point light source."


-- | Returns the ray, as it went through a portal. The incoming ray should
-- have its origin at the intersection point.
--
-- Currently it does not perform any ray rotation - only the origin is
-- affected.
getPortalDest :: Primitive -> Ray -> Ray
getPortalDest (Portal _ off) ray = makeRay (subV (rayOrigin ray) off) (rayDirection ray)
getPortalDest _              _   = error "Not a portal."


--------------------------------------------------------------------------------
-- PRIMITIVE MATERIAL PROPERTIES
--------------------------------------------------------------------------------

-- | Returns the material of some primitive.
getMaterial :: Primitive -> Material
getMaterial (AABox _ mat)       = mat
getMaterial (Composite _ _ _ _) = noMaterial
getMaterial (Plane _ _ mat)     = mat
getMaterial (Sphere _ _ mat)    = mat
getMaterial (PointLight prim _) = getMaterial prim
getMaterial (Portal prim _)     = getMaterial prim


-- | Returns the color of the primitive's material.
getMaterialColor :: Primitive -> ColorVector
getMaterialColor prim = matColor (getMaterial prim)

-- | Returns the density of the primitve.
getDensity :: Primitive -> Double
getDensity prim = matDensity (getMaterial prim)

-- | Returns the diffuse rating of a primitive.
getDiffuse :: Primitive -> Double
getDiffuse prim = matDiffusion (getMaterial prim)

-- | Returns the reflection rating of a primitive.
getReflection :: Primitive -> Double
getReflection prim = matReflection (getMaterial prim)

-- | Returns the refraction rating of a primitive.
getRefraction :: Primitive -> Double
getRefraction prim = matRefraction (getMaterial prim)

-- | Returns the refraction index of a primitive.
getRefrIndex :: Primitive -> Double
getRefrIndex prim = matRefrIndex (getMaterial prim)

-- | Returns the shininess rating of a primitive.
getShininess :: Primitive -> Int
getShininess prim = matShininess (getMaterial prim)

-- | Returns the specular reflection rating of a primitive.
getSpecular :: Primitive -> Double
getSpecular prim = matSpecular (getMaterial prim)


--------------------------------------------------------------------------------
-- TEXTURE COLOR CALCULATIONS
--------------------------------------------------------------------------------

-- | Returns the color of primitive's texture at specified point. It is
-- assumed that the said point lays on the primitive.
getTextureColor :: Primitive -> Vector3 -> ColorVector
getTextureColor prim@(AABox _ _)    pt = boxTexColor prim pt
getTextureColor (PointLight prim _) pt = sphereTexColor prim pt
getTextureColor prim@(Plane _ _ _)  pt = planeTexColor prim pt
getTextureColor prim@(Sphere _ _ _) pt = sphereTexColor prim pt
getTextureColor _                   _  = (1.0, 1.0, 1.0)


-- | Returns the color of a texture of a box at the specified point.
boxTexColor :: Primitive -> Vector3 -> ColorVector
boxTexColor (AABox box mat) (x, y, z) = texColor
    where
    ((a1,b1,c1), (a2,b2,c2)) = boxCorners box

    (u, v) =    if (floatsEqual a1 x) then ((z-c1) / (c2-c1), (y-b1) / (b1-b2))
           else if (floatsEqual a2 x) then ((z-c1) / (c2-c1), (y-b1) / (b1-b2))
           else if (floatsEqual b1 y) then ((x-a1) / (a2-a1), (z-c1) / (c2-c1))
           else if (floatsEqual b2 y) then ((x-a1) / (a2-a1), (z-c1) / (c2-c1))
           else if (floatsEqual c1 z) then ((x-a1) / (a2-a1), (y-b1) / (b1-b2))
           else                            ((x-a1) / (a2-a1), (y-b1) / (b1-b2))

    (tW, tH) = textureSize (matTexture mat)
    texColor = texelColor (matTexture mat) (u * (fromIntegral tW)) (v * (fromIntegral tH))


-- | Returns the color of a texture of a plane at the specified point.
planeTexColor :: Primitive -> Vector3 -> ColorVector
planeTexColor (Plane nrm _ mat) pt = texColor
    where
    (x, y, z) = pt

    xAxis = normalizeV (crossVector (0, 1, 0) nrm)
    yAxis = normalizeV (crossVector xAxis (multScV (-1) nrm))

    (u, v) = if (nrm == (0, 1, 0)) || (nrm == (0, -1, 0))
             then (x, z)
             else (dotProduct xAxis pt, dotProduct yAxis pt)

    (tW, tH) = textureSize (matTexture mat)
    texColor = texelColor (matTexture mat) (u * (fromIntegral tW) / 16) (v * (fromIntegral tH) / 16)


-- | Returns the color of a texture of a sphere at the specified point.
sphereTexColor :: Primitive -> Vector3 -> ColorVector
sphereTexColor (Sphere ctr rad mat) pt = texColor
    where
    (x, y, z) = multScV (1.0 / rad) (subV pt ctr)
    u = 1.0 - ((atan2 x z) / (2 * pi) + 0.5)
    v = 1.0 - ((asin y) / pi + 0.5)

    (tW, tH) = textureSize (matTexture mat)
    texColor = texelColor (matTexture mat) (u * (fromIntegral tW)) (v * (fromIntegral tH))
