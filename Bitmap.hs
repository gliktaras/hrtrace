--------------------------------------------------------------------------------
-- |
-- Module: Bitmap
--
-- Defines structures to represent a bitmap, as well as basic functions to
-- manipulate them. Currently the bitmap is implemented as an array of color
-- values.
--
-- IMPORTANT!
--
-- Due to the way arrays are implemented in Haskell, introduction of any change
-- into a bitmap will result in a full copy of the array, containing the pixel
-- values. So, setting every pixel individually in a 100x100 bitmap will result
-- in 10000 copies.
--
-- To prevent this, I introduced the 'setPixels' function. It modifies multiple
-- pixels at the same time, which copies the pixel array only once. This is why
-- it makes sense to accumulate changes before applying them and why drawing
-- functions in 'BitmapTools' return lists of 'BmpPxValue'.
--
--------------------------------------------------------------------------------

module Bitmap (
    -- * Types
    Bitmap,
    BmpPixel,
    BmpPxValue,

    -- * Bitmap creation
    makeBitmap',    -- :: Color -> Int -> Int -> Bitmap
    makeBitmap,     -- :: Int -> Int -> Bitmap

    -- * Bitmap manipulation
    bitmapSize,     -- :: Bitmap -> (Int, Int)
    getPixelColor,  -- :: Bitmap -> BmpPixel -> Color
    setPixels,      -- :: Bitmap -> [BmpPxValue] -> Bitmap

    bitmap2list     -- :: Bitmap -> [[(Int, Int, Int)]]

    ) where


import Common

import Data.Array


--------------------------------------------------------------------------------
-- BITMAP TYPE
--------------------------------------------------------------------------------

-- | This type represents a bitmap (i.e. a matrix of colors).
newtype Bitmap = BMP (Array (Int,Int) Color)
                 deriving (Eq, Read, Show)

-- | A type, which denotes a location on a bitmap.
type BmpPixel = (Int, Int)

-- | A type, which denotes how the given pixel should be modified.
type BmpPxValue = (BmpPixel, Color)


--------------------------------------------------------------------------------
-- BITMAP CREATION
--------------------------------------------------------------------------------

-- | Creates a bitmap with specified dimensions. All pixels are set to the given
-- color.
makeBitmap' :: Color -> Int -> Int -> Bitmap
makeBitmap' color width height = BMP (array bounds pixels)
    where
    wd = width - 1
    ht = height - 1
    bounds = ((0, 0), (wd, ht))
    pixels = zip [(w,h) | h <- [0..ht], w <- [0..wd]] (cycle [color])

-- | Creates a bitmap with specified dimensions. All pixels are initially black.
makeBitmap :: Int -> Int -> Bitmap
makeBitmap width height = makeBitmap' black width height


--------------------------------------------------------------------------------
-- FUNCTIONS ON BITMAP
--------------------------------------------------------------------------------

-- | Returns the dimensions of the given bitmap.
bitmapSize :: Bitmap -> (Int, Int)
bitmapSize (BMP pxArr) = (w + 1, h + 1)
    where
    ((_,_), (w,h)) = bounds pxArr


-- | Returns the color of the specified pixel.
getPixelColor :: Bitmap -> BmpPixel -> Color
getPixelColor (BMP pxArr) px = pxArr ! px

-- | Sets the given pixels to their specified colors. This operation is quite
-- expensive - accumulate changes before introducing them.
setPixels :: Bitmap -> [BmpPxValue] -> Bitmap
setPixels (BMP pxArr) changes = BMP (pxArr // changes)


-- | Returns the bitmap as a list of lists of tuples, which represent rows and
-- colors.
bitmap2list :: Bitmap -> [[(Int, Int, Int)]]
bitmap2list (BMP pxArr) = [[ color2tuple (pxArr ! (x, y)) | x <- [0..w]] | y <- [0..h] ]
    where
    ((_,_), (w,h)) = bounds pxArr
