--------------------------------------------------------------------------------
-- |
-- Module: Main
--
-- Contains code to launch the application.
--
--------------------------------------------------------------------------------

module Main where

import CompEntry
import TestRenders

import Data.List (isPrefixOf)
import System.Environment (getArgs)


--------------------------------------------------------------------------------
-- ENTRY POINT
--------------------------------------------------------------------------------

-- | Parses the command line parameters and returns the appropriate values.
--
-- There has got to be an easier way.
parseArgs :: [String] -> (Int, Bool, Bool) -> (Int, Bool, Bool)
parseArgs []     params = params
parseArgs (x:xs) (sz, aa, tex)
    | isPrefixOf "--size=" x = parseArgs xs ((read (drop 7 x))::Int, aa, tex)
    | x == "--no-aa"         = parseArgs xs (sz, False, tex)
    | x == "--no-textures"   = parseArgs xs (sz, aa, False)
    | otherwise              = parseArgs xs (sz, aa, tex)


-- | The main function.
main :: IO ()
main = do
    args <- getArgs

    if ("--render-tests" `elem` args)
      then do
        putStrLn "Outputting all test renderings..."
        renderTest01
        renderTest02
        renderTest03
        renderTest04
        renderTest05
        renderTest06
        renderTest07
        putStrLn "Done."
      else do
        let (imgSize, useAA, useTex) = parseArgs args (1000, True, True)
        putStrLn  "Outputting the competition entry image."
        putStrLn ("Size is " ++ show imgSize ++ "x" ++ show imgSize ++ ", "
               ++ "antialiasing is " ++ (if useAA then "on" else "off") ++ ", "
               ++ "textures are " ++ (if useTex then "on" else "off") ++ ".")
        infForum imgSize useAA useTex
        putStrLn "Done."
