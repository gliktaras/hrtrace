--------------------------------------------------------------------------------
-- |
-- Module: Renderer
--
-- Contains structures and functions that allow to render a scene.
--
--------------------------------------------------------------------------------

module Renderer (
    render      -- :: Scene -> (Int, Int) -> Bitmap
    ) where

import Bitmap
import Common
import Material
import Primitives
import Scene

import Control.Parallel.Strategies
import Data.List (zip4)


--------------------------------------------------------------------------------
-- RENDERING FUNCTIONS
--------------------------------------------------------------------------------

-- | Traces a ray in the specified scene. Arguments are: scene to render, ray to
-- trace in the said scene, current refraction index and current recursion
-- depth. It returns a pair, where the first value is the color of the current
-- ray and the second is ray's next intersection.
traceRay :: Scene -> Ray -> Double -> Int -> (Vector3, Intersection)
traceRay scene ray refrIdx depth
    | (getPrimList scene) == [] = (skyboxCol, makeMiss)
    | depth >= maxDepth         = (skyboxCol, makeMiss)
    | otherwise                 = let cIntr = closestIntr scene ray
                                  in  if (isMiss cIntr)
                                        then (skyboxCol, makeMiss)
                                        else calcColor cIntr
    where
    -- Bisecting scene properties.
    ambLight  = getAmbLight scene
    maxDepth  = getMaxDepth scene
    skyboxCol = getSkyboxColor scene

    -- Function, which calculates the color of the ray at current intersection.
    calcColor cIntr = if isLight prim
                        then (primMatColor, cIntr)
                      else if isPortal prim
                        then traceRay scene (getPortalDest prim (makeRay intrPt dir)) refrIdx (depth + 1)
                      else (finalColor, cIntr)
        where
        -- Incoming ray.
        orig = rayOrigin ray
        dir  = rayDirection ray

        -- Information about closest object intersection.
        prim   = intrPrimitive cIntr
        dist   = intrDistance cIntr
        normal = intrNormal cIntr
        intrPt = intrPointPos ray dist

        -- Primitive's color at current point.
        primMatColor = getMaterialColor prim
        primTexColor = getTextureColor prim intrPt
        primColor    = multV primMatColor primTexColor

        -- Properties of primitive's material.
        primDensity  = getDensity prim
        primDiffuse  = getDiffuse prim
        primSpecular = getSpecular prim
        primReflect  = getReflection prim
        primRefract  = getRefraction prim
        primRefrIdx  = getRefrIndex prim
        primShine    = getShininess prim

        -- Collecting point light data.
        lights    = getPointLightList scene
        lightMat  = map getMaterialColor lights
        lightDirs = [normalizeV (subV (getPtLightSource lt) intrPt) | lt <- lights]
        shades    = [calcShade lt lDir | (lt, lDir) <- zip lights lightDirs]
        lightData = zip4 lights lightMat lightDirs shades

        -- Calculating the color of the current intersection.
        ambient    = ambientLighting
        diffuse    = diffuseLighting lightData
        specular   = specularLighting lightData
        reflected  = reflectedRayColor
        refracted  = refractedRayColor

        finalColor = addListV [ambient, diffuse, specular, reflected, refracted]


        -- Calculates shade of a light source.
        calcShade light lightDir = if clIntr == light then 1.0 else 0.0
            where
            newRay = makeRay (addV intrPt (multScV epsilon lightDir)) lightDir
            clIntr = intrPrimitive (closestIntr scene newRay)


        -- Calculates the ambient lighting component of intersection's color.
        ambientLighting = multScV ambLight primColor

        -- Calculates the diffuse lighting component of intersection's color.
        diffuseLighting [] = (0.0, 0.0, 0.0)
        diffuseLighting ((light, lightCol, lightDir, shade):ls) =
            if ltValue > 0.0
              then addV curColor (diffuseLighting ls)
              else diffuseLighting ls
            where
            ltValue     = (dotProduct normal lightDir) * primDiffuse
            curRawColor = multScV (ltValue * shade) lightCol
            curColor    = multV primColor curRawColor

        -- Calculates the specular lighting component of intersection's color.
        specularLighting [] = (0.0, 0.0, 0.0)
        specularLighting ((light, lightCol, lightDir, shade):ls) =
            if ltValue > 0.0
              then addV curColor (specularLighting ls)
              else specularLighting ls
            where
            ltRefl    = vectorReflection normal lightDir
            ltValue   = dotProduct dir ltRefl
            specValue = (ltValue ^ primShine) * shade * primSpecular
            curColor  = multScV specValue lightCol

        -- Calculates the color of the reflected ray.
        reflectedRayColor =
            if primReflect > 0.0
              then reflCol
              else (0.0, 0.0, 0.0)
            where
            reflDir  = vectorReflection normal dir
            reflRay  = makeRay (addV intrPt (multScV epsilon reflDir)) reflDir
            rawColor = fst (traceRay scene reflRay refrIdx (depth + 1))
            reflCol  = multV primMatColor (multScV primReflect rawColor)

        -- Calculates the color of the refracted ray.
        refractedRayColor =
            if (primRefract > 0.0) && (cosT2 > 0.0) && (isHit refrIntr)
              then refrColor
              else (0.0, 0.0, 0.0)
            where
            refrRatio = refrIdx / primRefrIdx
            cosI      = -(dotProduct normal dir)
            cosT2     = 1.0 - refrRatio * refrRatio * (1.0 - cosI * cosI)

            newRefrIdx = if (isInsideHit cIntr) then 1.0 else primRefrIdx

            refrDir = addV (multScV refrRatio dir) (multScV (refrRatio * cosI - (sqrt cosT2)) normal)
            refrRay = makeRay (addV intrPt (multScV epsilon refrDir)) refrDir
            (refrRawColor, refrIntr) = traceRay scene refrRay newRefrIdx (depth + 1)

            rayLength      = distanceV (intrPt) (intrPoint refrRay refrIntr)
            (aX, aY, aZ)   = multScV (primDensity * (-rayLength)) primMatColor
            transparentCol = (exp aX, exp aY, exp aZ)

            refrColor = multScV primRefract (multV refrRawColor transparentCol)


-- | Renders the scene as a bitmap with specified dimensions.
render :: Scene -> (Int, Int) -> Bitmap
render scene (w,h) = result
    where
    -- Extracting camera data.
    camera  = getCamera scene
    camOrig = camOrigin camera
    scrUL   = camULCorner camera
    (scrWidth, scrHeight) = camDimensions camera

    -- Extracting scene data
    sSamples    = getAASamples scene
    sSamplesInv = 1.0 / (fromIntegral (sSamples ^ 2))

    -- Deltas for ray calculation
    deltaX = multScV (1 / (fromIntegral w)) scrWidth
    deltaY = multScV (1 / (fromIntegral h)) scrHeight

    -- Rendering the scene.
    pixels  = [ (x,y) | y <- [0..h-1], x <- [0..w-1] ]
    colors  = concat (parMap rnf (\y -> [getPixelColor x y | x <- [0..w-1]]) [0..h-1])
    changes = zip pixels colors
    result  = setPixels (makeBitmap w h) changes

    -- Calculates the color of a specified pixel.
    getPixelColor x y = vector2color pixelColor
        where
        x1 = fromIntegral x
        y1 = fromIntegral y

        partialRays = [getRay (x1 + a) (y1 + b) | (a, b) <- ssGrid]
        accColor    = addListV [fst (traceRay scene r 1.0 0) | r <- partialRays]
        pixelColor  = multScV sSamplesInv accColor

    -- Returns a ray, pointing from camera's original position to some point in
    -- screen plane.
    getRay x y = makeRay camOrig rayDir
        where
        scrPt  = addListV [scrUL, multScV y deltaY, multScV x deltaX]
        rayDir = normalizeV (subV scrPt camOrig)

    -- A grid, used in selection of partial rays. Uses rotated grid to select
    -- samples.
    ssOffset = 0.5 / (fromIntegral sSamples) - 0.5
    ssGrid = [let x1 = (((fromIntegral a) / (fromIntegral sSamples)) + ssOffset)
                  y1 = (((fromIntegral b) / (fromIntegral sSamples)) + ssOffset)
              in  ((x1 * cos30 - y1 * sin30), (x1 * sin30 + y1 * cos30))
              | a <- [1..sSamples], b <- [1..sSamples] ]

    cos30 = 0.8660254037844387
    sin30 = 0.5

