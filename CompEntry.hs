--------------------------------------------------------------------------------
-- |
-- Module: CompEntry
--
-- INF1 competition entry module. It contains all the code to render the
-- (approximation of) informatics forum.
--
--------------------------------------------------------------------------------

module CompEntry (
    infForum    -- :: Int -> Bool -> IO ()
    ) where

import Bitmap
import BitmapIO
import Common
import Material
import Primitives
import Renderer
import Scene
import Textures


--------------------------------------------------------------------------------
-- SUPPLEMENTARY FUNCTIONS
--------------------------------------------------------------------------------

-- | Creates one level of exterior detail of Informatics Forum building along
-- the X axis. The arguments, in order, are the starting X coordinate, range of
-- Y coordinates, Z axis offset the amount of detail segments, level of the
-- building and material of the detail.
bdgDecor_xAxis :: Double -> Double -> Double -> Double -> Int -> Int -> Material -> [Primitive]
bdgDecor_xAxis xStart yLow yHigh zOff count level mat =
    makeBlocks (scanl (+) xStart (concat (replicate count sizes)))
    where
    sizes = if (level `mod` 2 == 0) then [7, 2, 3, 6] else [3, 6, 7, 2]

    makeBlocks []       = []
    makeBlocks [_]      = []
    makeBlocks (x:y:xs) = (makeAABox (makeBox (x, yLow, zOff) (y, yHigh, zOff + 5)) mat) : makeBlocks xs


-- | Creates one level of exterior detail of Informatics Forum building along
-- the Z axis. The arguments, in order, are the starting Z coordinate, range of
-- Y coordinates, X axis offset the amount of detail segments, level of the
-- building and material of the detail.
bdgDecor_zAxis :: Double -> Double -> Double -> Double -> Int -> Int -> Material -> [Primitive]
bdgDecor_zAxis zStart yLow yHigh xOff count level mat =
    makeBlocks (scanl (+) (zStart + sOff) (tail (concat (replicate count sizes))))
    where
    sizes = if (level `mod` 2 == 0) then [6, 3, 2, 7] else [2, 7, 6, 3]
    sOff  = head sizes

    makeBlocks []       = []
    makeBlocks [_]      = []
    makeBlocks (x:y:xs) = (makeAABox (makeBox (xOff, yLow, x) (xOff + 5, yHigh, y)) mat) : makeBlocks xs


-- | Creates columns along the X axis. The arguments, in order, are range of X
-- coordinates, range of Y coordinates, Z axis offset, column width, column
-- count and column material.
columns_xAxis :: Double -> Double -> Double -> Double -> Double -> Double -> Int -> Material -> [Primitive]
columns_xAxis xLow xHigh yLow yHigh zOff colWd count mat = makeColumns 0
    where
    colSize   = ((xHigh - xLow) / (fromIntegral count))
    colOffset = (colSize - colWd) / 2

    makeColumns x
        | x >= count = []
        | otherwise  = let colX = xLow + (fromIntegral x) * colSize + colOffset
                       in  (makeAABox (makeBox (colX, yLow, zOff) (colX + colWd, yHigh, zOff + colWd)) mat) : (makeColumns (x+1))


-- | Creates columns along the Z axis. The arguments, in order, are X axis
-- offset, range of Y coordinates, range of Z coordinates, column width, column
-- count and column material.
columns_zAxis :: Double -> Double -> Double -> Double -> Double -> Double -> Int -> Material -> [Primitive]
columns_zAxis xOff yLow yHigh zLow zHigh colWd count mat = makeColumns 0
    where
    colSize   = ((zHigh - zLow) / (fromIntegral count))
    colOffset = (colSize - colWd) / 2

    makeColumns x
        | x >= count = []
        | otherwise  = let colZ = zLow + (fromIntegral x) * colSize + colOffset
                       in  (makeAABox (makeBox (xOff, yLow, colZ) (xOff + colWd, yHigh, colZ + colWd)) mat) : (makeColumns (x+1))


--------------------------------------------------------------------------------
-- THE RENDERING CODE
--------------------------------------------------------------------------------

-- | This function renders the Informatics Forum. The arguments specify the
-- size of the resulting image, whether antialiasing should be enabled and
-- whether textures should be used.
infForum :: Int -> Bool -> Bool -> IO ()
infForum imgSize useAA useTex = do
    -- Textures
    cloudTex   <- importFromBMP "textures/clouds.bmp"
    groundTex  <- importFromBMP "textures/tile3.bmp"
    logoTex    <- importFromBMP "textures/forumLogo.bmp"
    shutterTex <- importFromBMP "textures/shutter.bmp"
    wallTex    <- importFromBMP "textures/forumWall.bmp"
    windowTex  <- importFromBMP "textures/window.bmp"
    woodTex    <- importFromBMP "textures/woodPanels.bmp"


    -- Materials of the rendering
    let concreteMat = solidMaterial (makeRGB 140 140 140)
    let floorSepMat = solidMaterial (makeRGB 244 244 244)
    let railingMat  = solidMaterial (makeRGB   0   0   0)
    let whiteMat    = solidMaterial (makeRGB 244 244 244)

    -- There's gotta be an easier way to do this.
    let groundMat = if useTex
                      then textureMaterial (makeBitmapTex groundTex defaultTexMeta)
                      else solidMaterial (makeRGB 130 140 140)
    let infForumMat = if useTex
                        then textureMaterial (makeBitmapTex logoTex defaultTexMeta)
                        else solidMaterial (makeRGB 244 244 244)
    let shutterMat = if useTex
                       then textureMaterial (makeBitmapTex shutterTex defaultTexMeta)
                       else solidMaterial (makeRGB 110 125 125)
    let skyMat = if useTex
                   then textureMaterial (makeBitmapTex cloudTex (makeTexMeta (0, 0) (500000, 500000)))
                   else solidMaterial (makeRGB 135 205 235)
    let wallMat = if useTex
                    then textureMaterial (makeBitmapTex wallTex defaultTexMeta)
                    else solidMaterial (makeRGB 255 228 181)
    let windowMat = if useTex
                      then textureMaterial (makeBitmapTex windowTex (makeTexMeta (0, 0) (0.1, 1)))
                      else solidMaterial (makeRGB 0 0 100)
    let woodMat = if useTex
                    then textureMaterial (makeBitmapTex woodTex defaultTexMeta)
                    else solidMaterial (makeRGB 140  80  40)


    let scene = [ {-= WORLD OBJECTS ==========================================-}

                  -- Ground and sky planes
                  makePlane (0,  1, 0)       0 groundMat,
                  makePlane (0, -1, 0) 1000000 skyMat,

                  -- Sun
                  makePointLight (-400, 700, -300) white,


                  {-= FLOOR SEPARATORS =======================================-}

                  -- Exclusive to front section
                  makeAABox (makeBox (127,  25,  0) (273,  27, 100)) floorSepMat,
                  makeAABox (makeBox (  0,  91,  0) (273,  93, 227)) floorSepMat,
                  makeAABox (makeBox (  0, 113,  0) (273, 115, 227)) floorSepMat,
                  makeAABox (makeBox (  0, 135,  0) (273, 137, 227)) floorSepMat,
                  -- Exclusive to back section
                  makeAABox (makeBox (0,  91, 366) (200,  93, 544)) floorSepMat,
                  makeAABox (makeBox (0, 113, 366) (200, 115, 526)) floorSepMat,
                  makeAABox (makeBox (0, 135, 366) (200, 137, 544)) floorSepMat,
                  makeAABox (makeBox (0, 157, 366) (200, 159, 526)) floorSepMat,
                  makeAABox (makeBox (0, 179, 366) (200, 181, 544)) floorSepMat,
                  -- Separators along the length of the building
                  makeAABox (makeBox (0, 25, 37) (273, 27, 544)) floorSepMat,
                  makeAABox (makeBox (0, 47,  0) (273, 49, 544)) floorSepMat,
                  makeAABox (makeBox (0, 69,  0) (273, 71, 526)) floorSepMat,

                  {-= WINDOWS OF THE BUILDING ================================-}

                  -- Windows on the right side of the front section
                  makeAABox (makeBox (127,   0, 1) (271,  25, 3)) windowMat,
                  makeAABox (makeBox (127,  27, 1) (271,  47, 3)) windowMat,
                  makeAABox (makeBox ( 38,  49, 1) (271,  69, 3)) windowMat,
                  makeAABox (makeBox ( 38,  71, 1) (271,  91, 3)) windowMat,
                  makeAABox (makeBox ( 38,  93, 1) (271, 113, 3)) windowMat,
                  makeAABox (makeBox ( 38, 115, 1) (271, 135, 3)) windowMat,
                  -- Windows on the left side of the front section and along the
                  -- building on 1 and 2 levels.
                  makeAABox (makeBox (1, 27, 36) (3,  47, 543)) windowMat,
                  makeAABox (makeBox (1, 49, 11) (3,  69, 525)) windowMat,
                  makeAABox (makeBox (1, 71,  7) (3,  91, 226)) windowMat,
                  makeAABox (makeBox (1, 93, 11) (3, 113, 226)) windowMat,
                  makeAABox (makeBox (1, 115, 7) (3, 135, 226)) windowMat,

                  -- Windows on the left side of the back section
                  makeAABox (makeBox (1,   0, 421) (5,  25, 493)) windowMat,
                  makeAABox (makeBox (1,  71, 367) (2,  91, 525)) windowMat,
                  makeAABox (makeBox (1,  93, 367) (2, 113, 525)) windowMat,
                  makeAABox (makeBox (1, 115, 367) (2, 135, 525)) windowMat,
                  makeAABox (makeBox (1, 137, 367) (2, 157, 525)) windowMat,
                  makeAABox (makeBox (1, 159, 367) (2, 179, 525)) windowMat,
                  -- Windows on the right side of the back section
                  makeAABox (makeBox (1,  71, 367) (200,  91, 369)) windowMat,
                  makeAABox (makeBox (1,  93, 367) (200, 113, 369)) windowMat,
                  makeAABox (makeBox (1, 115, 367) (200, 135, 369)) windowMat,
                  makeAABox (makeBox (1, 137, 367) (200, 157, 369)) windowMat,
                  makeAABox (makeBox (1, 159, 367) (200, 179, 369)) windowMat,

                  {-= BUILDING CORNERS =======================================-}

                  -- Front corner of the front section
                  makeAABox (makeBox (0,  49, 0) (8,  69, 4)) wallMat,
                  makeAABox (makeBox (0,  71, 0) (8,  91, 4)) wallMat,
                  makeAABox (makeBox (0,  93, 0) (8, 113, 4)) wallMat,
                  makeAABox (makeBox (0, 115, 0) (8, 135, 4)) wallMat,
                  -- Right corner of the front section
                  makeAABox (makeBox (271,   0, 0) (273,  25, 5)) wallMat,
                  makeAABox (makeBox (271,  27, 0) (273,  47, 5)) wallMat,
                  makeAABox (makeBox (271,  49, 0) (273,  69, 5)) wallMat,
                  makeAABox (makeBox (271,  71, 0) (273,  91, 5)) wallMat,
                  makeAABox (makeBox (271,  93, 0) (273, 113, 5)) wallMat,
                  makeAABox (makeBox (271, 115, 0) (273, 135, 5)) wallMat,

                  -- Front corner of the back section
                  makeAABox (makeBox (0.01,  49, 366.01) (2,  69, 370)) wallMat,
                  makeAABox (makeBox (0.01,  71, 366.01) (2,  91, 370)) wallMat,
                  makeAABox (makeBox (0.01,  93, 366.01) (2, 113, 370)) wallMat,
                  makeAABox (makeBox (0.01, 115, 366.01) (2, 135, 370)) wallMat,
                  -- Left corner of the back section / leftmost corner
                  makeAABox (makeBox (0,  49, 538) (2,  91, 544)) wallMat,
                  makeAABox (makeBox (0,  93, 540) (2, 135, 544)) wallMat,
                  makeAABox (makeBox (0,  93, 526) (2, 135, 532)) wallMat,
                  makeAABox (makeBox (0, 137, 538) (2, 179, 544)) wallMat,

                  {-= ROOF BLOCKS ============================================-}

                  -- Front section roof
                  makeAABox (makeBox (-0.5, 137, -0.5) (273.5, 142, 227.5)) whiteMat,
                  -- Middle section roof
                  makeAABox (makeBox (-0.5, 71, 227) (100, 76, 366)) whiteMat,
                  makeAABox (makeBox ( 5  , 76, 227) (100, 78, 366)) whiteMat,
                  makeAABox (makeBox (-0.5, 78, 227) (100, 80, 366)) whiteMat,
                  -- Back section roof
                  makeAABox (makeBox (-0.5, 181, 363.5) (200, 186, 544.5)) whiteMat,

                  {-= SHUTTERS ===============================================-}

                  -- Shutter on the left of the front section
                  makeAABox (makeBox (-2,  71, 202) (5, 135, 204)) whiteMat,
                  makeAABox (makeBox (-2,  71, 225) (5, 135, 227)) whiteMat,
                  makeAABox (makeBox (-2,  71, 202) (5,  73, 227)) whiteMat,
                  makeAABox (makeBox (-2, 133, 202) (5, 135, 227)) whiteMat,
                  makeAABox (makeBox (-2,  73, 204) (5, 133, 214)) shutterMat,

                  -- Shutter on the left of the back section in middle levels
                  makeAABox (makeBox (-2,  71, 502) (-2, 135, 504)) whiteMat,
                  makeAABox (makeBox (-2,  71, 524) (-2, 135, 526)) whiteMat,
                  makeAABox (makeBox (-2,  71, 502) (-2,  73, 526)) whiteMat,
                  makeAABox (makeBox (-2, 133, 502) (-2, 135, 526)) whiteMat,
                  makeAABox (makeBox (-2,  73, 504) (-2, 133, 514)) shutterMat,

                  makeAABox (makeBox (0, 93, 496) (5, 113, 499)) wallMat,

                  -- Shutter on the right of the back section
                  makeAABox (makeBox (2, 137, 366) (5, 179, 368)) whiteMat,
                  makeAABox (makeBox (9, 137, 365.99) (19, 179, 368)) shutterMat,


                  {-= ENTRANCE INTO THE FORUM ================================-}

                  -- Informatics forum wall
                  makeAABox (makeBox ( 0   , 25,  0   ) (127, 47, 4)) whiteMat,
                  makeAABox (makeBox ( 0.01, 25, -0.01) ( 91, 47, 0)) infForumMat,
                  makeAABox (makeBox (91   , 27, -0.01) (127, 47, 1)) windowMat,

                  -- "Inside" area behind the columns in front
                  -- Concrete sections
                  makeAABox (makeBox ( 0,  0, 35  ) ( 10, 47, 37)) whiteMat,
                  makeAABox (makeBox ( 0, 45,  0  ) ( 10, 47, 37)) whiteMat,
                  makeAABox (makeBox (20, 25,  0.1) (127, 27, 40)) whiteMat,
                  -- Walls from wood panels
                  makeAABox (makeBox (  0.1,  0  , 34.9) (127, 47,  37)) woodMat,
                  makeAABox (makeBox (  0.1, 44.9,  0.1) (127, 47,  37)) woodMat,
                  makeAABox (makeBox (125  ,  0  ,  0.1) (127, 47, 127)) woodMat,
                  makeAABox (makeBox ( 91  , 24.9,  0.1) (127, 27,  37)) woodMat,
                  makeAABox (makeBox (  0.1, 24.9,  0.1) (127, 27,   4)) woodMat,
                  -- Entrance and a window behind the forum wall
                  makeAABox (makeBox (20, 27,  0.1 ) ( 22, 45, 40)) windowMat,
                  makeAABox (makeBox (92,  4, 34.89) (126, 24, 36)) windowMat,

                  {-= BALCONIES IN FRONT =====================================-}

                  -- Additional brick segments next to balconies
                  makeAABox (makeBox (35,  49, 0) (37,  69, 3)) wallMat,
                  makeAABox (makeBox (35,  71, 0) (37,  91, 3)) wallMat,
                  makeAABox (makeBox (35,  93, 0) (37, 113, 3)) wallMat,
                  makeAABox (makeBox (35, 115, 0) (37, 135, 3)) wallMat,

                  -- Balcony interior
                  makeAABox (makeBox (34.99, 47, 2  ) (37, 137, 10)) whiteMat,
                  makeAABox (makeBox ( 2   , 47, 9.9) (10, 137, 11)) woodMat,
                  -- Windows in every level
                  makeAABox (makeBox (10,  49, 10) (35,  69, 11)) windowMat,
                  makeAABox (makeBox (10,  71, 10) (35,  91, 11)) windowMat,
                  makeAABox (makeBox (10,  93, 10) (35, 113, 11)) windowMat,
                  makeAABox (makeBox (10, 115, 10) (35, 135, 11)) windowMat,

                  -- Railings
                  makeAABox (makeBox ( 8.8, 57, 2) (35, 58, 3)) railingMat,
                  makeAABox (makeBox (17.8, 49, 2) (18, 58, 3)) railingMat,
                  makeAABox (makeBox (25.8, 49, 2) (26, 58, 3)) railingMat,
                  makeAABox (makeBox (34.8, 49, 2) (35, 58, 3)) railingMat,

                  makeAABox (makeBox ( 8.8, 79, 2) (35, 80, 3)) railingMat,
                  makeAABox (makeBox (17.8, 71, 2) (18, 80, 3)) railingMat,
                  makeAABox (makeBox (25.8, 71, 2) (26, 80, 3)) railingMat,
                  makeAABox (makeBox (34.8, 71, 2) (35, 80, 3)) railingMat,

                  makeAABox (makeBox ( 8.8, 101, 2) (35, 102, 3)) railingMat,
                  makeAABox (makeBox (17.8,  93, 2) (18, 102, 3)) railingMat,
                  makeAABox (makeBox (25.8,  93, 2) (26, 102, 3)) railingMat,
                  makeAABox (makeBox (34.8,  93, 2) (35, 102, 3)) railingMat,

                  makeAABox (makeBox ( 8.8, 123, 2) (35, 124, 3)) railingMat,
                  makeAABox (makeBox (17.8, 115, 2) (18, 124, 3)) railingMat,
                  makeAABox (makeBox (25.8, 115, 2) (26, 124, 3)) railingMat,
                  makeAABox (makeBox (34.8, 115, 2) (35, 124, 3)) railingMat,

                  {-= BASE OF THE BUILDING (RIGHT SIDE) ======================-}

                  -- Concrete segment below ground level on the right.
                  makeAABox (makeBox (124.99, 0,  0) (273, 4, 50)) concreteMat,
                  makeAABox (makeBox ( 91   , 0,  2) (127, 4, 50)) concreteMat,
                  makeAABox (makeBox ( 91   , 0,  0) (127, 3, 50)) concreteMat,
                  makeAABox (makeBox ( 91   , 0, -2) (127, 2, 50)) concreteMat,
                  makeAABox (makeBox ( 91   , 0, -4) (127, 1, 50)) concreteMat,

                  -- Column base
                  makeAABox (makeBox (-10   , 0  ,  0   ) (91   , 4  , 10)) whiteMat,
                  makeAABox (makeBox (-10   , 0  , -4   ) (91   , 1.9,  0)) whiteMat,
                  makeAABox (makeBox ( -9.99, 1.9, -3.99) (91   , 2  ,  0)) woodMat,
                  makeAABox (makeBox (-10   , 0  , -4   ) ( 1.7 , 4  ,  0)) whiteMat,
                  makeAABox (makeBox ( 14.36, 0  , -4   ) (17.75, 4  ,  0)) whiteMat,
                  makeAABox (makeBox ( 27.33, 0  , -4   ) (30.72, 4  ,  0)) whiteMat,
                  makeAABox (makeBox ( 41.70, 0  , -4   ) (49.88, 4  ,  0)) whiteMat,
                  makeAABox (makeBox ( 80.02, 0  , -4   ) (91   , 4  ,  0)) whiteMat,

                  {-= GROUND LEVEL DETAILS ===================================-}

                  -- Lecture theatre on the ground level
                  makeAABox (makeBox ( 0   , 0, 37) (30, 27, 165)) whiteMat,
                  makeAABox (makeBox (-0.01, 2, 39) ( 3, 23, 163)) windowMat,
                  -- A bar, which goes along the side of the building
                  makeAABox (makeBox (-2, 23, 183) (5, 25, 420)) whiteMat,
                  -- Wall section past the lecture theatre
                  makeAABox (makeBox (0, 0, 183) (30, 27, 237)) whiteMat,
                  -- Window behind the columns
                  makeAABox (makeBox (10, 0, 237) (15, 25, 330)) windowMat,
                  -- Window past the middle section gap
                  makeAABox (makeBox (0, 0, 402) (50, 25, 420)) windowMat,

                  -- Dugald Stewart building entrance details
                  makeAABox (makeBox ( 5,     0, 480) (50, 25, 528)) windowMat,
                  makeAABox (makeBox (-0.01, 27, 495) ( 2, 47, 544)) whiteMat,

                  {-= TOP LEVELS OF THE SECOND BUILDING ======================-}

                  -- Sorry about this
                  makeAABox (makeBox (-0.01, 137, 366) (2, 179, 372)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 374) (6, 179, 384)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 385) (6, 179, 387)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 388) (6, 179, 398)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 400) (6, 179, 406)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 409) (6, 179, 419)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 421) (6, 179, 423)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 426) (6, 179, 436)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 443) (6, 179, 445)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 448) (6, 179, 458)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 459) (6, 179, 461)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 463) (6, 179, 473)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 473) (6, 179, 477)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 478) (6, 179, 488)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 490) (6, 179, 492)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 493) (6, 179, 503)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 504) (6, 179, 510)) wallMat,
                  makeAABox (makeBox (-0.01, 137, 511) (6, 179, 521)) shutterMat,
                  makeAABox (makeBox (-0.01, 137, 521) (6, 179, 526)) wallMat,


                  {-= MISC ELEMENTS ==========================================-}

                  -- Element on 1st level near the entrance to forum.
                  makeAABox (makeBox (0, 27, 37) (5, 47, 40)) wallMat,
                  -- Element on the ground level in the back section.
                  makeAABox (makeBox (0, 0, 492) (5, 25, 495)) wallMat ]


                  {-= EXTERIOR DECORATION ====================================-}

                  -- Exterior details of the right side of the front section.
                  ++ (bdgDecor_xAxis 127   4  25 0  8 0 wallMat)
                  ++ (bdgDecor_xAxis 127  27  47 0  8 1 wallMat)
                  ++ (bdgDecor_xAxis  37  49  69 0 13 2 wallMat)
                  ++ (bdgDecor_xAxis  37  71  91 0 13 3 wallMat)
                  ++ (bdgDecor_xAxis  37  93 113 0 13 4 wallMat)
                  ++ (bdgDecor_xAxis  37 115 135 0 13 5 wallMat)
                  -- Exterior details of the left side of the front section and
                  -- the whole building.
                  ++ (bdgDecor_zAxis 40  27  47 0 26 1 wallMat)
                  ++ (bdgDecor_zAxis  4  49  69 0 29 2 wallMat)
                  ++ (bdgDecor_zAxis  4  71  91 0 11 3 wallMat)
                  ++ (bdgDecor_zAxis  4  93 113 0 11 4 wallMat)
                  ++ (bdgDecor_zAxis  4 115 135 0 11 5 wallMat)
                  -- Exterior details of the left side of the back section.
                  ++ (bdgDecor_zAxis 418   0  25 0 4 0 wallMat)
                  ++ (bdgDecor_zAxis 364  71  91 0 8 3 wallMat)
                  ++ (bdgDecor_zAxis 364  93 113 0 7 4 wallMat)
                  ++ (bdgDecor_zAxis 364 115 135 0 8 5 wallMat)
                  -- Exterior details of the right side of the back section.
                  ++ (bdgDecor_xAxis 2 71 91 366.01 2 3 whiteMat)
                  ++ (bdgDecor_xAxis 2 93 113 366.01 2 4 whiteMat)
                  ++ (bdgDecor_xAxis 2 115  135 366.01 3 5 whiteMat)
                  ++ (bdgDecor_xAxis 20 137 157 366.01 3 6 whiteMat)
                  ++ (bdgDecor_xAxis 20 159 179 366.01 3 7 whiteMat)

                  {-= COLUMNS ================================================-}

                  -- Columns in front section
                  ++ (columns_xAxis 0 91 0 25 1.7 1.4 19 whiteMat)
                  -- Columns in the middle section
                  ++ (columns_zAxis 1.7 0 25 237   246   1.4 2  whiteMat)
                  ++ (columns_zAxis 1.7 0 25 250.5 291.0 1.4 9  whiteMat)
                  ++ (columns_zAxis 1.7 0 25 304.5 327.8 1.4 5  whiteMat)
                  -- Columns on the very left in the back section
                  ++ (columns_zAxis 1.7 0 25 496 544 1.4 13 whiteMat)
                  --}


    -- I used these cameras to fine-tune element placement.
    --let camera = makeCamera (  50,  15,  -30) ( 50,  15,   0)
    --let camera = makeCamera ( 250,  50, -100) (250,  50,   0)
    --let camera = makeCamera (-100,  75,  200) (  0,  75, 200)
    --let camera = makeCamera ( -50,  15,   20) (  0,  25,  30)
    --let camera = makeCamera (  20, 120,  -50) ( 10, 120,   0)
    --let camera = makeCamera (-100,  50,  544) (  0,  50, 544)
    --let camera = makeCamera (-100, 100,  210) (  0, 100, 210)
    --let camera = makeCamera (  50,  10, -100) ( 50,  10,   0)
    --let camera = makeCamera (-150,  20,  450) (  0,  20, 450)
    --let camera = makeCamera (  50,  20,   10) ( 90,  20,  10)
    --let camera = makeCamera (-150, 100,  450) (  0, 100, 450)
    --let camera = makeCamera (  20, 100,  266) ( 20, 100, 366)

    let aaSamples = if useAA then 3 else 1

    let camera  = makeCamera (-100, 40, -100) (-5,50,-5)
    let options = makeSceneOpts 0.35 16 black aaSamples
    let bitmap  = render (makeScene options camera scene) (imgSize, imgSize)

    exportAsBMP bitmap "infForum.bmp"
