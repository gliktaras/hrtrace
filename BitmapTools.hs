--------------------------------------------------------------------------------
-- |
-- Module: BitmapTools
--
-- Contains bitmap manipulation functions. Every drawing function comes in two
-- flavors: one to draws on a bitmap directly and another one two returns a
-- list of 'BmpPxValue', which allows to accumulate changes before commiting
-- them. Reasons for this are explained in documentation of module 'Bitmap'.
--
-- If error "Error in array index" is thrown, it means that you are trying to
-- draw outside of the bitmap.
--
--------------------------------------------------------------------------------

module BitmapTools (
    -- * Drawing functions on bitmap
    drawFillRect,   -- :: Bitmap -> Color -> Rectangle Int -> Bitmap
    drawLine,       -- :: Bitmap -> Color -> BmpPixel -> BmpPixel -> Bitmap
    drawPoint,      -- :: Bitmap -> Color -> BmpPixel -> Bitmap
    drawRect,       -- :: Bitmap -> Color -> Rectangle Int -> Bitmap

    -- * Accumulating functions on bitmap
    drawFillRect',  -- :: Color -> Rectangle Int -> [BmpPxValue]
    drawLine',      -- :: Color -> BmpPixel -> BmpPixel -> [BmpPxValue]
    drawPoint',     -- :: Color -> BmpPixel -> [BmpPxValue]
    drawRect',      -- :: Color -> Rectangle Int -> [BmpPxValue]

    ) where

import Bitmap
import BitmapIO
import Common


--------------------------------------------------------------------------------
-- DRAWING FUNCTIONS
--------------------------------------------------------------------------------

-- | Returns a list of pixel changes, which would draw a point on a bitmap.
drawPoint' :: Color -> BmpPixel -> [BmpPxValue]
drawPoint' newColor pt = [(pt, newColor)]

-- | Draws a point on the bitmap.
drawPoint :: Bitmap -> Color -> BmpPixel -> Bitmap
drawPoint bitmap newColor pt = setPixels bitmap (drawPoint' newColor pt)


-- | Returns a list of pixel changes, which would draw a rectangle on a bitmap.
drawRect' :: Color -> Rectangle Int -> [BmpPxValue]
drawRect' rectColor rect = newPoints
    where
    ((ulx, uly), (lrx, lry)) = rectCorners rect
    topBound    = [ ((x, uly), rectColor) | x <- [ulx..lrx] ]
    bottomBound = [ ((x, lry), rectColor) | x <- [ulx..lrx] ]
    leftBound   = [ ((ulx, y), rectColor) | y <- [uly..lry] ]
    rightBound  = [ ((lrx, y), rectColor) | y <- [uly..lry] ]
    newPoints   = topBound ++ bottomBound ++ leftBound ++ rightBound

-- | Draws a rectangle on a bitmap.
drawRect :: Bitmap -> Color -> Rectangle Int -> Bitmap
drawRect bitmap rectColor rect = setPixels bitmap (drawRect' rectColor rect)


-- | Returns a list of pixel changes, which would draw a filled rectangle on a
-- bitmap.
drawFillRect' :: Color -> Rectangle Int -> [BmpPxValue]
drawFillRect' rectColor rect = newPoints
    where
    ((ulx, uly), (lrx, lry)) = rectCorners rect
    newPoints = [ ((x, y), rectColor) | x <- [ulx..lrx], y <- [uly..lry] ]

-- | Draws a filled rectangle on the bitmap.
drawFillRect :: Bitmap -> Color -> Rectangle Int -> Bitmap
drawFillRect bitmap rectColor rect =
    setPixels bitmap (drawFillRect' rectColor rect)


-- Returns a list of pixel changes, which would draw a line on a bitmap. The
-- line is specified by its endpoints.
--
-- Line is obtained using the Bresenham's line algorithm.
drawLine' :: Color -> BmpPixel -> BmpPixel -> [BmpPxValue]
drawLine' lineColor (p1x, p1y) (p2x, p2y) = newPoints
    where
    steepLine = (abs (p2y - p1y)) > (abs (p2x - p1x))

    (x1, y1, x2, y2) = if steepLine
                         then if p1y > p2y
                                then (p2y, p2x, p1y, p1x)   -- Steep, right-to-left.
                                else (p1y, p1x, p2y, p2x)   -- Steep, left-to-right.
                         else if p1x > p2x
                                then (p2x, p2y, p1x, p1y)   -- Normal, right-to-left.
                                else (p1x, p1y, p2x, p2y)   -- Normal, left-to-right.
    deltaX = x2 - x1
    deltaY = abs (y2 - y1)
    yStep  = if y1 < y2 then 1 else (-1)

    -- Line plotting function.
    drawLine' cX cY err
        | cX > x2   = []
        | err < 0   = drawLine' cX (cY + yStep) (err + deltaX)
        | otherwise = (curCoords, lineColor) : (drawLine' (cX + 1) cY (err - deltaY))
        where
        curCoords = if steepLine then (cY, cX) else (cX, cY)

    newPoints = drawLine' x1 y1 (deltaX `div` 2)


-- | Draws a line of the specified color on the bitmap. The line is specified
-- by its end points.
drawLine :: Bitmap -> Color -> BmpPixel -> BmpPixel -> Bitmap
drawLine bitmap lineColor p1 p2 = setPixels bitmap (drawLine' lineColor p1 p2)


--------------------------------------------------------------------------------
-- TESTING
--------------------------------------------------------------------------------

-- | Tests the point drawing function. If test is successful, test_drawPoint.bmp
-- should contain a 100x100 sized checker pattern.
testDrawPoint :: IO()
testDrawPoint = do
    let points = [(x,y) | x <- [0..99], y <- [0..99], ((x + y) `mod` 2) == 0]
    let bmp    = foldr (\(x,y) b -> drawPoint b white (x,y)) (makeBitmap 100 100) points
    exportAsBMP bmp "test_drawPoint.bmp"
    return ()

-- | Tests the rectangle drawing function. If test is successful,
-- test_drawRect.bmp should contains a 3D-ish pattern from rectangles.
testDrawRect :: IO()
testDrawRect = do
    let rects = [ makeRect (x,x) (x+50, x+50) | x <- [10,12..40] ]
    let bmp   = foldr (\r b -> drawRect b white r) (makeBitmap 100 100) rects
    exportAsBMP bmp "test_drawRect.bmp"
    return ()

-- | Tests the filled rectangle drawing function. If the test is successful, file
-- test_drawFillRect.bmp should contain a nice gradient pattern from squares.
testDrawFillRect :: IO()
testDrawFillRect = do
    let rects = [ (makeRect (x,x) (x+50,x+50), makeRGB (x*6) (x*6) (x*6)) | x <- [10,12..40] ]
    let bmp   = foldr (\(r,c) b -> drawFillRect b c r) (makeBitmap 100 100) rects
    exportAsBMP bmp "test_drawFillRect.bmp"
    return ()

-- | Tests the line drawing function. On success, file test_drawLine.bmp should
-- contain a star.
testDrawLine :: IO ()
testDrawLine = do
    let lines = concat [ [((50,50), (10,x)), ((50,50), (90,x)),
                          ((50,50), (x,10)), ((50,50), (x,90))] | x <- [10,20..90] ]
    let bmp   = foldr (\(x,y) b -> drawLine b white x y) (makeBitmap 100 100) lines
    exportAsBMP bmp "test_drawLine.bmp"
    return ()

