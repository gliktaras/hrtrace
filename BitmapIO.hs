--------------------------------------------------------------------------------
-- |
-- Module: BitmapIO
--
-- Defines function to import and export bitmaps from/to various image formats.
-- So far only BMP is supported.
--
--------------------------------------------------------------------------------

module BitmapIO (
    -- * Export functions
    exportAsBMP,    -- :: Bitmap -> String -> IO ()

    -- * Import functions
    importFromBMP   -- :: String -> IO Bitmap

    ) where

import Bitmap
import Common

import Data.Char
import Data.List


--------------------------------------------------------------------------------
-- BMP IMPORT/EXPORT
--------------------------------------------------------------------------------

-- | Creates a bitmap from a BMP file. This function can only process unindexed,
-- uncompressed 24-bit BMP files.
importFromBMP :: String -> IO Bitmap
importFromBMP filename = do rawData <- readFile filename
                            return (convertRawData rawData)
    where
    convertRawData rawData = if ((isSuffixOf ".bmp" filename) && (magicNumber == "BM"))
                               then if ((bpp == 24) && (compression == 0) && (paletteSize == 0))
                                      then bitmap
                                      else error "Unsupported BMP file given."
                               else error "Not a BMP file given."
        where
        -- Correctness checks.
        magicNumber = slice rawData 0 1                             -- Must equal "BM"
        bpp         = rawBinToInt (reverse (slice rawData 28 29))   -- Must equal 24
        compression = rawBinToInt (reverse (slice rawData 30 33))   -- Must equal 0
        paletteSize = rawBinToInt (reverse (slice rawData 46 49))   -- Must equal 0

        -- Information about bitmap.
        dataOffset = rawBinToInt (reverse (slice rawData 10 13))
        imgWidth   = rawBinToInt (reverse (slice rawData 18 21))
        imgHeight  = rawBinToInt (reverse (slice rawData 22 25))

        (wd, ht) = (imgWidth - 1, imgHeight - 1)

        -- Converting raw data into a bitmap.
        bitmap = setPixels (makeBitmap imgWidth imgHeight) (processBMP 0 (imgHeight-1) (drop dataOffset rawData))

        offsetSize = (4 - (3 * imgWidth) `mod` 4) `mod` 4
        processBMP w h xs
            | w == imgWidth = processBMP 0 (h-1) (drop offsetSize xs)
            | h < 0         = []
            | otherwise     = ((w, h), rgbFromBinary (take 3 xs)) : (processBMP (w+1) h (drop 3 xs))

        -- Converts BMP pixel description into a Color object.
        rgbFromBinary [b,g,r] = makeRGB (ord r) (ord g) (ord b)


-- | Saves the given bitmap as a specified BMP file.
exportAsBMP :: Bitmap -> String -> IO ()
exportAsBMP bitmap filename = do writeFile filename (header ++ rawData)
    where
    (width, height) = bitmapSize bitmap
    wd = width - 1
    ht = height - 1

    -- A standard BMP header.
    rowSize  = 4 * ceiling (fromIntegral (24 * width) / 32)
    fileSize = 54 + rowSize * height

    bppField      = reverse (intToRawBin 2 24)
    fileSizeField = reverse (intToRawBin 4 fileSize)
    heightField   = reverse (intToRawBin 4 height)
    rawSizeField  = reverse (intToRawBin 4 (fileSize - 54))
    widthField    = reverse (intToRawBin 4 width)

    header = "\x42\x4D"             -- Magic number (BM).
          ++ fileSizeField          -- Size of the BMP file.
          ++ "\x00\x00"             -- Reserved.
          ++ "\x00\x00"             -- Reserved.
          ++ "\x36\x00\x00\x00"     -- Image data offset (54).
          ++ "\x28\x00\x00\x00"     -- Number of bytes in header (40).
          ++ widthField             -- Width of the bitmap.
          ++ heightField            -- Height of the bitmap.
          ++ "\x01\x00"             -- Number of color planes in use.
          ++ bppField               -- Bytes per pixel (24).
          ++ "\x00\x00\x00\x00"     -- Compression method (none).
          ++ rawSizeField           -- Size of raw bitmap data.
          ++ "\x13\x0B\x00\x00"     -- Horizontal resolution of the image (2835).
          ++ "\x13\x0B\x00\x00"     -- Vertical resolution of the image (2835).
          ++ "\x00\x00\x00\x00"     -- Number of colors in the palette.
          ++ "\x00\x00\x00\x00"     -- Important colors (all of them).

    -- Raw image data.
    rowOffsetNum = (4 - (3 * width) `mod` 4) `mod` 4
    rowOffset    = replicate rowOffsetNum '\x00'

    rawData = concat [ concat [rawPixel x y | x <- [0..wd]] ++ rowOffset | y <- reverse [0..ht] ]

    -- Converts the color of a pixel to raw binary (little-endian order).
    rawPixel x y = blue ++ green ++ red
        where
        (r, g, b) = color2tuple (getPixelColor bitmap (x,y))
        red   = intToRawBin 1 r
        green = intToRawBin 1 g
        blue  = intToRawBin 1 b


--------------------------------------------------------------------------------
-- INTEGER CONVERSION TO/FROM RAW BINARY
--------------------------------------------------------------------------------

-- | Returns the given integer as a raw binary field of specified size.
-- Big-endian order is used to order bytes - use function reverse, to obtain the
-- Little-endian order.
intToRawBin :: Int -> Int -> String
intToRawBin fieldSize val = if rawStrLen >= fieldSize
                            then rawStr
                            else (replicate (fieldSize - rawStrLen) '\x00') ++ rawStr
    where
    rawStr    = toRawBin val
    rawStrLen = length rawStr

    toRawBin 0 = ""
    toRawBin x = toRawBin restDig ++ [chr cDig]
        where
        cDig    = x `mod` 256
        restDig = x `div` 256


-- | Converts a binary string into a corresponding integer.
rawBinToInt :: String -> Int
rawBinToInt xs
    | length xs > 4 = error "Cannot convert such a large value."
    | otherwise     = fromRawBin (reverse xs)
    where
    fromRawBin []     = 0
    fromRawBin (x:xs) = (ord x) + 256 * (fromRawBin xs)
