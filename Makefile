SOURCES = Bitmap.hs BitmapIO.hs BitmapTools.hs Common.hs CompEntry.hs Main.hs Material.hs Primitives.hs Renderer.hs Scene.hs TestRenders.hs Textures.hs

# Commands for building.
build:
	ghc -O2 -funfolding-use-threshold=16 -funbox-strict-fields -optc-ffast-math -fvia-c -optc-O3 -fexcess-precision -threaded --make -o raytracer $(SOURCES)

# Commands for profiling.
profile:
	echo "" > core.txt
	ghc -O2 -funfolding-use-threshold=16 -funbox-strict-fields -fvia-c -optc-ffast-math -optc-O3 -fexcess-precision -prof -auto-all -caf-all -fforce-recomp -ddump-simpl --make -o raytracer $(SOURCES) >> core.txt

# Documentation creation.
docs:
	mkdir html
	haddock -h -o ./html $(SOURCES)

# Cleanup.
clean:
	rm *.hi
	rm *.o
