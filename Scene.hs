--------------------------------------------------------------------------------
-- |
-- Module: Scene
--
-- Contains structures and functions, which define a renderable scene.
--
--------------------------------------------------------------------------------

module Scene (
    -- * Types
    SceneOpts,
    Scene,
    Camera,

    -- * Functions on SceneOpts
    makeSceneOpts,      -- :: Double -> Int -> Color -> Int -> SceneOpts

    -- * Functions on cameras
    makeCamera,         -- :: Vector3 -> Vector3 -> Camera

    camOrigin,          -- :: Camera -> Vector3
    camULCorner,        -- :: Camera -> Vector3
    camDimensions,      -- :: Camera -> (Vector3, Vector3)

    -- * Functions on scene
    makeScene,          -- :: SceneOpts -> Camera -> [Primitive] -> Scene

    getAASamples,       -- :: Scene -> Int
    getAmbLight,        -- :: Scene -> Double
    getCamera,          -- :: Scene -> Camera
    getMaxDepth,        -- :: Scene -> Int
    getPointLightList,  -- :: Scene -> [Primitive]
    getPrimList,        -- :: Scene -> [Primitive]
    getSkyboxColor,     -- :: Scene -> ColorVector

    closestIntr         -- :: Scene -> Ray -> Intersection

    ) where

import Primitives
import Common

import Material

import Data.List


--------------------------------------------------------------------------------
-- SCENE PROPERTIES
--------------------------------------------------------------------------------

-- | Structure, which contains the properties of the scene. The arguments, in
-- order, are the level of ambient light, maximum depth of ray rendering,
-- color of the skybox and the amount of anti-aliasing samples.
data SceneOpts = ScOpts Double Int ColorVector Int
                 deriving (Eq, Read, Show)

-- | Creates a correct SceneOptions object. The arguments are the same as of
-- the 'ScOpts' constructor.
makeSceneOpts :: Double -> Int -> Color -> Int -> SceneOpts
makeSceneOpts ambLight maxDepth skyboxCol aaSamples
    | (ambLight < 0.0) = error "Invalid ambient light level."
    | (ambLight > 1.0) = error "Invalid ambient light level."
    | (maxDepth <= 0)  = error "Invalid depth value."
    | (aaSamples <= 0) = error "Invalid amount of AA samples."
    | otherwise        = ScOpts ambLight maxDepth (color2vector skyboxCol) aaSamples



--------------------------------------------------------------------------------
-- CAMERA
--------------------------------------------------------------------------------

-- | This type represents a camera, which is used to render the scene with.
-- Options of the constructor, in order, are the origin of the camera, the
-- location of the upper-left corner of the screen rectangle and two vectors,
-- which define the width and height of the screen rectangle.
data Camera = Cam Vector3 Vector3 Vector3 Vector3
              deriving (Eq, Read, Show)


-- | Creates a camera, which looks at some specified point from the origin.
makeCamera :: Vector3 -> Vector3 -> Camera
makeCamera camOrig camTarget = newCamera
    where
    -- Fixed origin of the camera (camera's origin and target points must not
    -- have equal Z coordinates).
    camOrig' = let (x,y,z)  = camOrig
                   (_,_,z1) = camTarget
               in  if (z == z1) then (x, y, z + epsilon) else (x, y, z)

    -- Distance to the target.
    tgtDist = lengthV (subV camTarget camOrig')
    -- Offsets to calculate the screen rectangle.
    scrOffX = tgtDist * 0.8
    scrOffY = tgtDist * 0.8

    -- Axis, which define the camera.
    zAxis = normalizeV (subV camTarget camOrig')
    xAxis = normalizeV (crossVector (0, 1, 0) zAxis)
    yAxis = normalizeV (crossVector xAxis (multScV (-1) zAxis))

    -- Corner coordinates of the screen rectangle.
    newUL = addListV [camTarget, multScV (-scrOffX) xAxis, multScV (scrOffY) yAxis]
    newLL = addListV [camTarget, multScV (-scrOffX) xAxis, multScV (-scrOffY) yAxis]
    newUR = addListV [camTarget, multScV (scrOffX) xAxis, multScV (scrOffY) yAxis]

    -- Vectors, representing the width and height of the screen rectangle.
    scrWdV = subV newUR newUL
    scrHtV = subV newLL newUL

    -- The new camera object.
    newCamera = Cam camOrig' newUL scrWdV scrHtV


-- | Returns the origin of the camera.
camOrigin :: Camera -> Vector3
camOrigin (Cam orig _ _ _) = orig

-- | Returns the coordinates of the upper-left corner of the screen
-- rectangle.
camULCorner :: Camera -> Vector3
camULCorner (Cam _ ul _ _) = ul

-- | Returns the vectors, which represent the width and height of the screen
-- rectangle as a pair.
camDimensions :: Camera -> (Vector3, Vector3)
camDimensions (Cam _ _ wd ht) = (wd, ht)


--------------------------------------------------------------------------------
-- COLLECTION OF PRIMITIVES
--------------------------------------------------------------------------------

-- | A structure, respresenting a collection of primitives. Represents
-- a collection of primitives a list.
data PrimStruct = PrimStr [Primitive] PrimTree
                  deriving (Eq, Read, Show)

-- | Type, representing a tree structure of primitives.
data PrimTree = PTreeLeaf (Box Double) [Primitive]
              | PTreeNode (Box Double) Double PrimTree PrimTree
                deriving (Eq, Read, Show)


-- | Returns the bounding box of some node of a primitives' tree.
nodeBounds :: PrimTree -> Box Double
nodeBounds (PTreeLeaf box _)     = box
nodeBounds (PTreeNode box _ _ _) = box


-- | Creates a collection of primitives from a list of primitives.
makePrimStruct :: [Primitive] -> PrimStruct
makePrimStruct xs = planes `seq` pTree `seq` PrimStr planes pTree
    where
    -- Primitives, grouped by type.
    planes    = filter isPlane xs
    nonPlanes = filter (not . isPlane) xs

    -- Creating the kd-tree
    pTree = case nonPlanes of
                [] -> PTreeLeaf (makeBox (0,0,0) (1,1,1)) []
                _  -> let sceneBounds = foldr1 joinBoxes (map getBoundingBox nonPlanes)
                      in  makePrimTree sceneBounds nonPlanes 0

    -- This function creates a kd-tree. Splitting plane is chosen with SAH
    -- optimisation.
    makePrimTree box ps d = if (length ps <= 5) || (d >= 20) || (cBoxCost < spCost)
                              then PTreeLeaf box ps
                              else PTreeNode box mid leftTree rightTree
        where
        ((x1,y1,z1), (x2,y2,z2)) = boxCorners box

        -- Cost of the current node.
        cBoxCost = (boxSurfaceArea box) * (fromIntegral (length ps))

        -- All potential split locations for this node.
        allSplits = sort (concatMap currentEnds ps)
        currentEnds p = case (d `mod` 3) of
                            0 -> [x1 - epsilon, x2 + epsilon]
                            1 -> [y1 - epsilon, y2 + epsilon]
                            2 -> [z1 - epsilon, z2 + epsilon]
            where
            ((x1, y1, z1), (x2, y2, z2)) = boxCorners (getBoundingBox p)

        -- Locating the best split location.
        (spCost, mid, leftBox, leftPs, rightBox, rightPs) = getBestSplit allSplits

        getBestSplit [] = (1e300, 0, makeBox (0,0,0) (0,0,0), [], makeBox (0,0,0) (0,0,0), [])
        getBestSplit (curPos:rest) =
            if curCost < bestCost
              then curSplit
              else bestSp
            where
            curSplit = (curCost, curPos, lBox, lPs, rBox, rPs)
            bestSp@(bestCost,_,_,_,_,_) = getBestSplit rest

            -- Left and right node boxes.
            (lBox, rBox) =
                case (d `mod` 3) of
                    0 -> (makeBox (x1,y1,z1) (curPos,y2,z2), makeBox (curPos,y1,z1) (x2,y2,z2))
                    1 -> (makeBox (x1,y1,z1) (x2,curPos,z2), makeBox (x1,curPos,z1) (x2,y2,z2))
                    2 -> (makeBox (x1,y1,z1) (x2,y2,curPos), makeBox (x1,y1,curPos) (x2,y2,z2))

            -- Primitives in left and right nodes.
            lPs = filter (\p -> boxIntersectsPrim p lBox)  ps
            rPs = filter (\p -> boxIntersectsPrim p rBox) ps

            -- Cost of the current split.
            lArea   = boxSurfaceArea lBox
            rArea   = boxSurfaceArea rBox
            lCount  = fromIntegral (length lPs)
            rCount  = fromIntegral (length rPs)
            curCost = 1.0 * (lArea * lCount + rArea * rCount)

        -- Final left and right sub-trees.
        leftTree  = makePrimTree leftBox leftPs (d+1)
        rightTree = makePrimTree rightBox rightPs (d+1)


-- | Returns the intersection object, which corresponds to the closest
-- intersection of the given ray (this is a private version of the
-- 'closestIntr' function below).
closestIntr' :: PrimStruct -> Ray -> Intersection
closestIntr' (PrimStr planes pTree) ray = planeIntr `min` treeIntr
    where
    -- Intersection with scene's planes.
    planeIntr = minimum (makeMiss : (concatMap (\p -> rayIntersectsPrim p ray) planes))
    -- Intersection with other primitives.
    treeIntr  = case (lineIntersectsBox (nodeBounds pTree) ray) of
                    [i,j] -> findTreeIntr pTree i j 0
                    _     -> makeMiss

    (ox, oy, oz) = rayOrigin ray

    -- This function traverses the kd-tree to find the closest ray-primitive
    -- intersection.
    findTreeIntr (PTreeLeaf _ []) _ _ _ = makeMiss
    findTreeIntr (PTreeLeaf _ ps) _ _ _ = minimum (makeMiss : concatMap (\p -> rayIntersectsPrim p ray) ps)
    findTreeIntr (PTreeNode box mid lTree rTree) intrClose intrFar d = result `seq` result
        where
        (cx, cy, cz) = intrClose
        (fx, fy, fz) = intrFar

        -- Intersection and origin coordinates in the required dimension.
        (iClosePos, iFarPos, rPos) = case (d `mod` 3) of
                                    0 -> (cx, fx, ox)
                                    1 -> (cy, fy, oy)
                                    2 -> (cz, fz, oz)
        -- Bounds of the childen nodes.
        leftBounds  = nodeBounds lTree
        rightBounds = nodeBounds rTree

        -- Heavily pruned descent into children nodes.
        result = if (iClosePos <= mid)
                   then if (iFarPos <= mid)
                          then findTreeIntr lTree intrClose intrFar (d+1)
                        else if (iFarPos >= mid) && (rPos >= mid)
                          then (findTreeIntr rTree intrMidRight intrFar (d+1))
                        else let res1 = (findTreeIntr lTree intrClose intrMidRight (d+1))
                                 res2 = (findTreeIntr rTree intrMidRight intrFar (d+1))
                             in  if (isHit res1) && (pointInBox (intrPoint ray res1) leftBounds)
                                   then res1
                                   else res1 `min` res2
                 else   -- iClosePos >= mid
                        if (iFarPos >= mid)
                          then findTreeIntr rTree intrClose intrFar (d+1)
                        else if (iFarPos <= mid) && (rPos <= mid)
                          then (findTreeIntr lTree intrMidLeft intrFar (d+1))
                        else let res1 = (findTreeIntr rTree intrClose intrMidLeft (d+1))
                                 res2 = (findTreeIntr lTree intrMidLeft intrFar (d+1))
                             in  if (isHit res1) && (pointInBox (intrPoint ray res1) rightBounds)
                                   then res1
                                   else res1 `min` res2

        -- Coordinates of the intersection with a splitting plane.
        intrMidLeft = case (lineIntersectsBox leftBounds ray) of
                        (x:_) -> x
                        _     -> error "Epsilon FFFFFFFUUUUUUUUU..."
        intrMidRight = case (lineIntersectsBox rightBounds ray) of
                        (x:_) -> x
                        _     -> error "Epsilon FFFFFFFUUUUUUUUU..."


--------------------------------------------------------------------------------
-- THE SCENE STRUCTURE
--------------------------------------------------------------------------------

-- | A structure, representing a renderable scene. Arguments represent the
-- rendering options of a scene, its camera, a collection of primitives, a
-- list of primitives and point light sources for quick access.
data Scene = Sc SceneOpts Camera PrimStruct [Primitive] [Primitive]
             deriving (Eq, Read, Show)


-- | Creates a scene structure. Takes scene properties, the camera and a list
-- of primitives as its arguments.
makeScene :: SceneOpts -> Camera -> [Primitive] -> Scene
makeScene opts cam prims = Sc opts cam (makePrimStruct prims) prims (filter isLight prims)


-- | Returns the amount of anti-aliasing samples to use for the specified scene.
getAASamples :: Scene -> Int
getAASamples (Sc (ScOpts _ _ _ aaSmp) _ _ _ _) = aaSmp

-- | Returns the level of ambient light of the specified scene.
getAmbLight :: Scene -> Double
getAmbLight (Sc (ScOpts ambLt _ _ _) _ _ _ _) = ambLt

-- | Returns the camera of a specified scene.
getCamera :: Scene -> Camera
getCamera (Sc _ cam _ _ _) = cam

-- | Returns the maximum ray rendering depth of the specified scene.
getMaxDepth :: Scene -> Int
getMaxDepth (Sc (ScOpts _ maxDepth _ _) _ _ _ _) = maxDepth

-- | Returns a list of point light sources in the scene.
getPointLightList :: Scene -> [Primitive]
getPointLightList (Sc _ _ _ _ ptLt) = ptLt

-- | Returns a list of primitives, which is present in the given scene.
getPrimList :: Scene -> [Primitive]
getPrimList (Sc _ _ _ ps _) = ps

-- | Returns the color of the skybox of the specified scene.
getSkyboxColor :: Scene -> ColorVector
getSkyboxColor (Sc (ScOpts _ _ skyCol _) _ _ _ _) = skyCol


-- | Returns the intersection object, which corresponds to the closest
-- intersection of the given ray
closestIntr :: Scene -> Ray -> Intersection
closestIntr (Sc _ _ pCol _ _) ray = closestIntr' pCol ray
