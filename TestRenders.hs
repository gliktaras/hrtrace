--------------------------------------------------------------------------------
-- |
-- Module: TestRenders
--
-- A module containing renderings, which I have used to test the features of
-- the raytracer.
--
--------------------------------------------------------------------------------

module TestRenders (
    renderTest01,   -- :: IO ()
    renderTest02,   -- :: IO ()
    renderTest03,   -- :: IO ()
    renderTest04,   -- :: IO ()
    renderTest05,   -- :: IO ()
    renderTest06,   -- :: IO ()
    renderTest07    -- :: IO ()
    ) where

import Bitmap
import BitmapIO
import Common
import Material
import Primitives
import Renderer
import Scene
import Textures


--------------------------------------------------------------------------------
-- SAMPLE SCENES
--------------------------------------------------------------------------------

-- | A simple scene to showcase rendering of spheres, reflections and diffuse
-- and specular illumination.
renderTest01 :: IO()
renderTest01 = do
    let scene  = [ makeSphere ( 1.0, -0.8, 3.0) 2.5 (makeMaterial (makeRGB 100 100  75) noTexture 0.2 0.8 0.6 0.0 1.0 1.0  15),
                   makeSphere (-5.5, -0.5, 7.0) 2.0 (makeMaterial (makeRGB 180 180 180) noTexture 0.1 0.9 1.0 0.0 1.0 1.0 300),
                   makePointLight (0.0, 5.0, 5.0) (makeRGB 180 180 255),
                   makePointLight (2.0, 5.0, 1.0) (makeRGB 180 180 220),
                   makePlane (0.0, 1.0, 0.0) 4.4 (makeMaterial (makeRGB 100 100 75) noTexture 1.0 0.0 0.5 0.0 1.0 1.0 20) ]

    let camera  = makeCamera (0, 0, -5) (0, 0, 0)
    let options = makeSceneOpts 0.0 16 black 3
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render01.bmp"


-- | This scene showcases axis-aligned boxes and free camera.
renderTest02 :: IO()
renderTest02 = do
    let boxColor    = makeRGB 160  70  40
    let pillarColor = makeRGB 140  40  70
    let planeColor  = makeRGB 180 170 220
    let sphereColor = makeRGB 120 140 160

    let scene = [ makeAABox (makeBox (-3.0,-3.0, 3.0) (-2.0, 3.0, 4.0)) (solidMaterial pillarColor),
                  makeAABox (makeBox ( 2.0,-3.0, 3.0) ( 3.0, 3.0, 4.0)) (solidMaterial pillarColor),
                  makeAABox (makeBox (-3.0,-3.0, 6.0) (-2.0, 3.0, 7.0)) (solidMaterial pillarColor),
                  makeAABox (makeBox ( 2.0,-3.0, 6.0) ( 3.0, 3.0, 7.0)) (solidMaterial pillarColor),
                  makeAABox (makeBox (-0.7,-2.8, 1.0) (-0.2,-2.3, 1.5)) (solidMaterial boxColor),
                  makeAABox (makeBox ( 0.3,-2.5, 0.5) ( 0.8,-2.0, 1.0)) (solidMaterial boxColor),
                  makeSphere (0.0, 0.0, 5.0) 1.0 (makeMaterial sphereColor noTexture 0.2 0.8 1.0 0.0 1.0 1.0 20),
                  makePlane (0.0, 1.0, 0.0) 3.0 (solidMaterial planeColor),
                  makePlane (1.0, 0.0, 0.0) 4.0 (solidMaterial planeColor),
                  makePointLight (0.0, 2.0, 5.0) white,
                  makePointLight (0.0,-2.0, 5.0) white ]

    let camera  = makeCamera (10, 13, 6) (0, 0, 5)
    let options = makeSceneOpts 0.0 16 black 3
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render02.bmp"


-- | Rendering test for portals.
renderTest03 :: IO()
renderTest03 = do
    let lambdaColor = makeRGB 255 215 0

    let scene = [ makeAABox (makeBox ( 0.0, 8.0, 0.0) ( 1.0, 9.0, 1.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 9.0, 1.0) ( 1.0,10.0, 2.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 7.0, 2.0) ( 1.0, 9.0, 3.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 5.0, 3.0) ( 1.0, 7.0, 4.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 3.0, 2.0) ( 1.0, 5.0, 3.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 1.0, 1.0) ( 1.0, 3.0, 2.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 0.0, 0.0) ( 1.0, 1.0, 1.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 3.0, 4.0) ( 1.0, 5.0, 5.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 1.0, 5.0) ( 1.0, 3.0, 6.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 0.0, 6.0) ( 1.0, 1.0, 7.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox ( 0.0, 1.0, 7.0) ( 1.0, 2.0, 8.0)) (solidMaterial lambdaColor),
                  makeAABox (makeBox (-9.0,-1.0,-1.0) ( 2.0, 0.0, 9.0)) (solidMaterial white),
                  makePointLight (15, 7, 3) white,
                  makePortal (makeAABox (makeBox (-14.0,-1.0,-1.0) (-15.0,12.0,30.0)) noMaterial) (-18.0, 0.0, 0.0) ]

    let camera  = makeCamera (10, 6, 14) (0, 5, 8)
    let options = makeSceneOpts 0.5 128 black 3
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render03.bmp"


-- | Another portal test.
renderTest04 :: IO ()
renderTest04 = do
    let frameColor = makeRGB 120 135 105
    let boxColor   = makeRGB 230  40  50

    let scene = [ makeAABox (makeBox ( 0.0, 0.0, 0.0) (10.0, 1.0,10.0)) (solidMaterial frameColor),
                  makeAABox (makeBox ( 0.0, 0.0, 0.0) ( 1.0,10.0,10.0)) (solidMaterial frameColor),
                  makeAABox (makeBox ( 0.0, 0.0, 0.0) (10.0,10.0, 1.0)) (solidMaterial frameColor),
                  makeAABox (makeBox ( 9.0, 1.0, 9.0) (10.0, 2.0,10.0)) (solidMaterial boxColor),
                  makeAABox (makeBox ( 9.0, 6.0, 9.0) (10.0, 7.0,10.0)) (solidMaterial boxColor),
                  makeAABox (makeBox ( 9.0, 3.5, 5.0) (10.0, 4.5, 6.0)) (solidMaterial boxColor),
                  makeAABox (makeBox ( 5.0, 3.5, 9.0) ( 6.0, 4.5,10.0)) (solidMaterial boxColor),
                  makePointLight (15.0, 11.0, 5.0) white,
                  makePortal (makeSphere (4.0, 4.0, 4.0) 4.0 noMaterial) (-6.0, 0.0, -6.0) ]

    let camera  = makeCamera (13.0, 5.0, 13.0) (3.0, 3.0, 3.0)
    let options = makeSceneOpts 0.1 256 black 3
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render04.bmp"


-- | A test, which shows off texturing capabilities.
renderTest05 :: IO ()
renderTest05 = do
    earthTex <- importFromBMP "textures/earth_small.bmp"
    tileTex1 <- importFromBMP "textures/tile1.bmp"
    tileTex2 <- importFromBMP "textures/tile2.bmp"

    {-- The original checker textures.
    let sphereTex = makeCheckerTex white blue   (makeTexMeta (0.0, 0.0) (0.2, 0.1))
    let boxTex    = makeCheckerTex black yellow (makeTexMeta (0.0, 0.0) (0.5, 0.5))
    let planeTex  = makeCheckerTex (makeRGB 0 160 100) (makeRGB 0 120 120) defaultTexMeta
    --}

    let sphereTex = makeBitmapTex earthTex defaultTexMeta
    let boxTex    = makeBitmapTex tileTex1 defaultTexMeta
    let planeTex  = makeBitmapTex tileTex2 defaultTexMeta

    let scene = [ makeSphere (6.0,10.0, 6.0) 2.5 (textureMaterial sphereTex),
                  makeAABox (makeBox (3.0, 1.0, 3.0) (7.0, 5.0, 7.0)) (textureMaterial boxTex),
                  makePlane (1.0, 0.0, 0.0) (-1.0) (makeMaterial gray planeTex  0.7 0.3 1.0 0.0 1.0 1.0 1000),
                  makePlane (0.0, 1.0, 0.0) ( 1.0) (makeMaterial gray noTexture 0.0 1.0 1.0 0.0 1.0 1.0 1000),
                  makePlane (0.0, 0.0, 1.0) (-1.0) (makeMaterial gray noTexture 0.0 1.0 1.0 0.0 1.0 1.0 1000),
                  makePointLight (20.0, 17.0, 20.0) white ]

    let camera  = makeCamera (14.0, 12.0, 14.0) (5.0, 4.0, 5.0)
    let options = makeSceneOpts 0.3 16 black 3
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render05.bmp"


-- | A test for composite objects.
renderTest06 :: IO ()
renderTest06 = do
    let cubeMat = solidMaterial (makeRGB 150 60 0)
    let wallMat = makeMaterial white noTexture 0.2 0.8 0.3 0.0 1.0 1.0 50

    let scene = [ makeComposite CompDiff
                    (makeComposite CompIntr
                       (makeAABox (makeBox (-10, -10, -10) (10, 10, 10)) cubeMat)
                       (makeSphere (0, 0, 0) 14.0 cubeMat))
                    (makeComposite CompUnion
                       (makeComposite CompUnion
                          (makeComposite CompUnion
                             (makeComposite CompUnion
                                (makeSphere (-10, -10, -10) 4.3 cubeMat)
                                (makeSphere (-10, -10,  10) 4.3 cubeMat))
                             (makeComposite CompUnion
                                (makeSphere (-10,  10, -10) 4.3 cubeMat)
                                (makeSphere (-10,  10,  10) 4.3 cubeMat)))
                          (makeComposite CompUnion
                             (makeComposite CompUnion
                                (makeSphere (10, -10, -10) 4.3 cubeMat)
                                (makeSphere (10, -10,  10) 4.3 cubeMat))
                             (makeComposite CompUnion
                                (makeSphere (10,  10, -10) 4.3 cubeMat)
                                (makeSphere (10,  10,  10) 4.3 cubeMat))))
                       (makeSphere (0, 0, 0) 13.8 cubeMat)),

                  makeAABox (makeBox (-100, -100, -100) (-50, 100, 100)) wallMat,
                  makeAABox (makeBox (-100, -100, -100) (100, -50, 100)) wallMat,
                  makeAABox (makeBox (-100, -100, -100) (100, 100, -50)) wallMat,

                  makeAABox (makeBox ( 50, -100, -100) (100, 100, 100)) wallMat,
                  makeAABox (makeBox (-100,  50, -100) (100, 100, 100)) wallMat,
                  makeAABox (makeBox (-100, -100,  50) (100, 100, 100)) wallMat,

                  makePointLight (0, 0, 0) white ]

    let camera  = makeCamera (30.0, 30.0, -49.0) (0.0, 0.0, 0.0)
    let options = makeSceneOpts 0.3 4 black 1
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render06.bmp"


-- | A rendering test for refraction.
renderTest07 :: IO ()
renderTest07 = do
    let scene = [ makePlane (0.0, 1.0, 0.0) 4.4 (makeMaterial (vector2color (0.4, 0.3, 0.3)) noTexture 1.0 0.8 0.0 0.0 1.0 0.15 20),
                  makePlane (0.4, 0.0, -1.0) 12.0 (makeMaterial (vector2color (0.5, 0.3, 0.5)) noTexture 0.6 0.0 0.0 0.0 1.0 0.15 20),
                  makePlane (0.0, -1.0, 0.0) 7.4 (makeMaterial (vector2color (0.4, 0.7, 0.7)) noTexture 0.5 0.0 0.0 0.0 1.0 0.15 20),

                  makePointLight (0.0, 5.0, 5.0) (vector2color (0.4, 0.4, 0.4)),
                  makePointLight (-3.0, 5.0, 1.0) (vector2color (0.6, 0.6, 0.8)),

                  makeSphere (-1.5, -3.8, 1.0) 1.5 (makeMaterial (vector2color (1.0, 0.4, 0.4)) noTexture 0.2 0.8 0.0 1.0 1.5 0.15 20),
                  makeSphere (2.0, 0.8, 3.0) 2.5 (makeMaterial (vector2color (0.7, 0.7, 1.0)) noTexture 0.2 0.8 0.2 1.0 1.3 0.15 20),
                  makeSphere (-5.5, -0.5, 7.0) 2.0 (makeMaterial (vector2color (0.7, 0.7, 1.0)) noTexture 0.1 0.8 0.5 0.0 1.3 0.15 20) ]

                ++ [ makeSphere (-4.5 + x*1.5, -4.3 + y*1.5, 10.0) 0.3 (makeMaterial (vector2color (0.3, 1.0, 0.4)) noTexture 0.6 0.6 0.0 0.0 1.0 0.15 20)
                    | x <- [0..7], y <- [0..6] ]

    let camera  = makeCamera (0.0, 0.0, -5.0) (0.0, 0.0, 0.0)
    let options = makeSceneOpts 0.0 16 black 1
    let bitmap  = render (makeScene options camera scene) (1000, 1000)

    exportAsBMP bitmap "test_render07.bmp"

