--------------------------------------------------------------------------------
-- |
-- Module: Textures
--
-- Defines various textures and methods to operate on them.
--
--------------------------------------------------------------------------------

module Textures (
    -- * Texture metadata
    TextureMeta,

    defaultTexMeta,     -- :: TextureMeta
    makeTexMeta,        -- :: (Double, Double) -> (Double, Double) -> TextureMeta

    -- * Textures
    Texture,

    noTexture,          -- :: Texture
    makeCheckerTex,     -- :: Color -> Color -> TextureMeta -> Texture
    makeBitmapTex,      -- :: Bitmap -> TextureMeta -> Texture

    texelColor,         -- :: Texture -> Double -> Double -> ColorVector
    textureSize,        -- :: Texture -> (Int, Int)

    ) where

import Bitmap
import Common

--------------------------------------------------------------------------------
-- TEXTURE METADATA
--------------------------------------------------------------------------------

-- | The metadata of a texture. Includes the amounts by which the texture
-- should be offset (the first pair) and scaled (the second pair).
--
-- To be replaced with a 2x2 transformation matrix.
data TextureMeta = TexMeta (Double, Double) (Double, Double)
                   deriving (Eq, Read, Show)


-- | Creates the texture metadata object. Arguments, in order, are x and y axis
-- offsets and x and y axis scale factors.
makeTexMeta :: (Double, Double) -> (Double, Double) -> TextureMeta
makeTexMeta (offX, offY) (scX, scY)
    | scX <= 0  = error "Texture scale factor cannot be non-positive."
    | scY <= 0  = error "Texture scale factor cannot be non-positive."
    | otherwise = TexMeta (offX, offY) (1.0 / scX, 1.0 / scY)

-- | Returns default metadata for a texture (no offsetting or scaling).
defaultTexMeta :: TextureMeta
defaultTexMeta = makeTexMeta (0.0, 0.0) (1.0, 1.0)


-- | Transforms a point on a texture to match the given settings.
transformPoint :: TextureMeta -> Double -> Double -> (Double, Double)
transformPoint (TexMeta (offX, offY) (scX, scY)) u v = (scX * u + offX, scY * v + offY)


--------------------------------------------------------------------------------
-- TEXTURE TYPE
--------------------------------------------------------------------------------

-- | The texture type. Different constructors allow to create a texture from
-- different sources.
data Texture =
    -- | No texture.
    NoTex
    -- | A checker pattern with 2x2 size with specified colors.
  | CheckerTex ColorVector ColorVector TextureMeta
    -- | A 2D raster texture.
  | BitmapTex Bitmap TextureMeta
    deriving (Eq, Read, Show)


-- | No texture constructor.
noTexture :: Texture
noTexture = NoTex

-- | Creates a checker pattern texture.
makeCheckerTex :: Color -> Color -> TextureMeta -> Texture
makeCheckerTex col1 col2 meta = CheckerTex (color2vector col1) (color2vector col2) meta

-- | Creates a texture from a bitmap.
makeBitmapTex :: Bitmap -> TextureMeta -> Texture
makeBitmapTex bitmap meta = BitmapTex bitmap meta


-- | Returns the size of the texture.
textureSize :: Texture -> (Int, Int)
textureSize NoTex              = (1, 1)
textureSize (CheckerTex _ _ _) = (2, 2)
textureSize (BitmapTex bmp _)  = bitmapSize bmp

-- | Returns the color of the texture at the specified point (u, v).
texelColor :: Texture -> Double -> Double -> ColorVector
texelColor NoTex                  u v = (1.0, 1.0, 1.0)
texelColor tex@(CheckerTex _ _ _) u v = checkerTexColor tex u v
texelColor tex@(BitmapTex _ _)    u v = bitmapTexColor tex u v


--------------------------------------------------------------------------------
-- TEXEL COLOR CALCULATIONS
--------------------------------------------------------------------------------

-- | Returns the color of the specified bitmap texture at the given point.
-- Also, applies bilinear filtering to the texture.
bitmapTexColor (BitmapTex bitmap meta) u v
    -- Apparently this works.
    | newU `seq` newV `seq` w `seq` h `seq` fU `seq` fV `seq` w1 `seq`
      w2 `seq` w3 `seq` w4 `seq` u1 `seq` v1 `seq` u2 `seq` v2 `seq` c1 `seq`
      c2 `seq` c3 `seq` c4 `seq` False = undefined
    | otherwise = texColor
    where
    (newU, newV) = transformPoint meta u v
    (w, h) = bitmapSize bitmap

    -- Fraction part of the coordinates
    fU = newU - (fromIntegral (floor newU))
    fV = newV - (fromIntegral (floor newV))

    -- Weight of each of the four sub-pixels
    w1 = (1 - fU) * (1 - fV)
    w2 = fU * (1 - fV)
    w3 = (1 - fU) * fV
    w4 = fU * fV

    -- Coordinates of each of the four sub-pixels
    u1 = (floor newU)     `mod` w
    v1 = (floor newV)     `mod` h
    u2 = (floor newU + 1) `mod` w
    v2 = (floor newV + 1) `mod` h

    -- Colors of each of the four sub-pixels.
    c1 = color2vector (getPixelColor bitmap (u1, v1))
    c2 = color2vector (getPixelColor bitmap (u2, v1))
    c3 = color2vector (getPixelColor bitmap (u1, v2))
    c4 = color2vector (getPixelColor bitmap (u2, v2))

    -- Weighted sum of colors of sub-pixels.
    texColor = addListV [ multScV w1 c1, multScV w2 c2, multScV w3 c3, multScV w4 c4 ]


-- | Returns the color of the specified checker texture at the given location.
checkerTexColor (CheckerTex col1 col2 meta) u v =
    if ((u' + v') `mod` 2 == 0) then col1 else col2
    where
    (newU, newV) = transformPoint meta u v
    u' = floor newU
    v' = floor newV
